import { http } from '@tauri-apps/api';
import { ResponseType } from '@tauri-apps/api/http';

const get = async (url: string): Promise<string> => {
	// @ts-ignore
	if (window.__TAURI__)
		return (
			await http.fetch<string>(url, {
				responseType: ResponseType.Text,
				method: 'GET',
			})
		).data;
	else return await (await fetch(url)).text();
};

const getBinary = async (url: string): Promise<ArrayBuffer> => {
	if (Object.keys(window).includes('__TAURI_METADATA__')) {
		const response = (
			await http.fetch<number[]>(url, {
				responseType: ResponseType.Binary,
				method: 'GET',
			})
		).data;
		const buffer = new Uint8Array(response);

		return buffer;
	} else return await (await fetch(url)).arrayBuffer();
};

export { get, getBinary };

// const data = new ArrayBuffer(
// 	(await http.fetch<number[]>('url', {
// 		responseType: ResponseType.Binary,
// 		method: 'GET',
// 	})).data
// );
