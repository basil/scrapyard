import { Feed } from './store';

/**
 * Get the count of unread posts in a feed
 */
const useUnreadPosts = (feed: Feed) =>
	feed.posts.reduce((prev, feed) => prev + (feed.unread ? 1 : 0), 0);

export default useUnreadPosts;
