import { arrow, ComputePositionConfig, Padding } from '@floating-ui/dom';
import { computePosition } from '@floating-ui/dom';
import { Accessor, createEffect } from 'solid-js';

const usePopover = (
	anchorRef: Accessor<HTMLElement>,
	popoverRef: Accessor<HTMLElement>,
	options?: Partial<ComputePositionConfig> & {
		arrow?: {
			element: Accessor<HTMLElement>;
			padding?: Padding;
		};
	}
) => {
	createEffect(async () => {
		const anchorEl = anchorRef();
		const popoverEl = popoverRef();

		if (anchorEl && popoverEl) {
			if (options.arrow) {
				const arrowEl = options.arrow.element();
				if (arrowEl)
					options.middleware.push(
						arrow(Object.assign(options, { element: arrowEl }))
					);
			}

			// Position popover
			const position = await computePosition(
				anchorRef(),
				popoverRef(),
				options
			);

			popoverEl.style.position = 'absolute';
			popoverEl.style.left = `${position.x}px`;
			popoverEl.style.top = `${position.y}px`;

			// Position arrow
			if (
				position.middlewareData.arrow !== undefined &&
				options.arrow !== undefined
			) {
				const arrow = options.arrow.element();
				const arrowData = position.middlewareData.arrow;

				if (arrowData.x) arrow.style.left = `${arrowData.x}px`;
				if (arrowData.y) arrow.style.top = `${arrowData.y}px`;
				arrow.style.position = 'absolute';
			}
		}
	});
};

export default usePopover;
