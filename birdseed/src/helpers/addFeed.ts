// TODO: Move library into this repo
import { parseRSS2 } from '../../../../node.js/rss/src/index';
import getIcon from './getIcon';
import { Post, setStore, store } from './store';
import { get } from './http';

const addFeed = (url: string) =>
	new Promise<string>(async (resolve, reject) => {
		try {
			if (store.feeds.find(feed => feed.url === url)) return reject('Error: Feed with same URL already exists');

			const data = await get(url);
			const feed = await parseRSS2(data, url);
			const id = store.feeds.length.toString();

			setStore('feeds', store.feeds.length, {
				id: store.feeds.length.toString(),
				title: feed.title,
				url,
				format: 'rss',
				icon: await getIcon(feed.image),
				lastChecked: Date.now(),
				posts: feed.items
					.map<Post>((item, i) => ({
						...item,
						title: item.title || 'Untitled',
						unread: true,
						author: item.author,
						published: item.published,
					}))
					.sort(
						(a, b) =>
							(b.published ? b.published.getTime() : 0) -
							(a.published ? a.published.getTime() : 0)
					),
			});

			resolve(id);
		} catch (e) {
			console.trace(e);
			reject(`Failed to add feed: ${e}`);
		}
	});

export default addFeed;
