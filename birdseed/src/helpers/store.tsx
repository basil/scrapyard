import {
	createContext,
	createEffect,
	ParentComponent,
	useContext,
} from 'solid-js';
import { createStore } from 'solid-js/store';
import superjson from 'superjson';

// Types for global state
type FeedFormat = 'rss' | 'atom';

type Post = {
	title: string;
	link?: string;
	author?: string;
	description?: string;
	published?: Date;
	unread: boolean;
};

type Feed = {
	url: string;
	id: string; // Unique ID
	format: FeedFormat;
	icon: string; // dataURI of icon, as a 64x64 png
	title: string;
	lastChecked: number; // Timestamp
	posts: Post[];
};

type Settings = {
	// refetchInterval: number;
	intro: boolean;
};

type Store = {
	feeds: Feed[];
	settings: Settings;
};

// Store
const [store, setStore] = createStore<Store>(
	(() => {
		try {
			return superjson.parse<Store>(localStorage.getItem('store'));
		} catch {
			return {
				feeds: [],
				settings: {
					intro: false,
				},
			};
		}
	})()
);
// Persist data
createEffect(() => localStorage.setItem('store', superjson.stringify(store)));

// Store context
const StoreContext = createContext<Store>();
// Wrapper function for useContext used in child components
const useStore = () => useContext(StoreContext)!;

// Context provider
const StoreProvider: ParentComponent = (props) => {
	return (
		<StoreContext.Provider value={store}>
			{props.children}
		</StoreContext.Provider>
	);
};

export { StoreProvider, useStore, store, setStore };
export type { Feed, Post };
