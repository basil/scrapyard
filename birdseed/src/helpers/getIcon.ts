import { getBinary } from './http';

/**
 * Fetch an 64x64 dataURI from a url, or return the default icon
 */
const getIcon = async (url: string) => {
	const data = await getBinary(url || '/no_icon.png');
	const blob = new Blob([data]);
	console.log(url, data);

	// Downscale image via canvas
	const canvas = document.createElement('canvas');
	canvas.height = 128;
	canvas.width = 128;
	const ctx = canvas.getContext('2d')!;

	// Create an object URL to bypass any potential CORS errors
	const img = document.createElement('img');
	img.src = URL.createObjectURL(blob);
	await img.decode();

	// TODO: Handle when an image isn't square (crop it)
	ctx.drawImage(img, 0, 0, 128, 128);
	URL.revokeObjectURL(img.src);

	return canvas.toDataURL();
};

export default getIcon;
