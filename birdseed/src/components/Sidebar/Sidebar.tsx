import { Inbox } from 'lucide-solid';
import { useLocation } from 'solid-app-router';
import { useStore } from '../../helpers/store';
import useUnreadPosts from '../../helpers/useUnreadPosts';
import Text from '../Text';
import SidebarItemLink from './Item/SidebarItemLink';
import SidebarItemNew from './Item/SidebarItemNew';
import Logo from './logo.svg';
import { createSignal, For } from 'solid-js';
import Icon from '../Icon';
import { autoAnimate } from 'solid-auto-animate';
import {
	ContextMenu,
	ContextMenuBoundary,
	ContextMenuPanel,
} from 'solid-headless';

autoAnimate;

const Sidebar = () => {
	const { feeds } = useStore();

	return (
		<nav class='z-20 flex h-auto w-29 min-w-29 flex-1 cursor-default select-none flex-col gap-3 bg-black-dark p-3'>
			<h3 class='flex gap-1 font-semibold text-white-bright'>
				<Logo />
				Birdseed
			</h3>

			<div>
				<Text label class='mb-1'>
					Feeds
				</Text>
				<div>
					<SidebarItemLink
						text='All'
						id='all'
						icon={
							<Inbox
								width={24}
								height={24}
								class='stroke-white-bright'
							/>
						}
						enabled={useLocation().pathname === '/feed/all'}
						unread={feeds.reduce(
							(prev, feed) => prev + useUnreadPosts(feed),
							0
						)}
					/>

					<div use:autoAnimate>
						<For each={feeds}>
							{(feed) => (
								<SidebarItemLink
									text={feed.title}
									id={feed.id}
									icon={<Icon src={feed.icon} />}
									enabled={
										useLocation().pathname ===
										`/feed/${feed.id}`
									}
									unread={useUnreadPosts(feed)}
									// reloading={feed.reloading}
								/>
							)}
						</For>
					</div>

					<SidebarItemNew />
				</div>
			</div>
		</nav>
	);
};

export default Sidebar;
