import { batch, Component, Show } from 'solid-js';
import { Plus } from 'lucide-solid';
import { Popover, PopoverButton, PopoverPanel } from 'solid-headless';
import { createSignal } from 'solid-js';
import Text from '../../Text';
import usePopover from '../../../helpers/usePopover';
import { offset } from '@floating-ui/dom';
import { Motion, Presence } from '@motionone/solid';
import SidebarItemBase from './SidebarItemBase';
import { createFormControl } from 'solid-forms';
import * as yup from 'yup';
import classNames from 'classnames';
import addFeed from '../../../helpers/addFeed';
import { useNavigate } from 'solid-app-router';

const SidebarItemNew: Component = () => {
	const [anchor, setAnchor] = createSignal<HTMLElement>();
	const [popper, setPopper] = createSignal<HTMLElement>();
	const [arrowEl, setArrow] = createSignal<HTMLElement>();
	const [loading, setLoading] = createSignal(false);
	const [open, setOpen] = createSignal(false);
	const navigator = useNavigate();
	const control = createFormControl('');

	control.markTouched(false);

	usePopover(anchor, popper, {
		placement: 'right',
		middleware: [offset(40)],
		arrow: {
			element: arrowEl,
			padding: 8,
		},
	});

	// Handle the submitting of the feed form, adding a new feed
	const onSubmit = (e: SubmitEvent) => {
		e.preventDefault();
		if (loading()) return;
		batch(() => {
			control.markTouched(false);
			control.setErrors(
				yup.string().url().required().isValidSync(control.value)
					? null
					: { message: 'Invalid URL' }
			);
		});
		if (control.errors) return;

		// Add to global state
		addFeed(control.value)
			.then((id) => {
				batch(() => {
					setOpen(false);
					setLoading(false);
					control.setValue('');
				});
				navigator(`/feed/${id}`);
			})
			.catch((err) => {
				control.setErrors({ message: err });
				setLoading(false);
			});
		setLoading(true);
	};

	return (
		<Popover
			isOpen={open()}
			onClose={() => {
				setOpen(false);
				batch(() => {
					control.setErrors({});
					control.setValue('');
				});
			}}
			onOpen={() => setOpen(true)}
		>
			<PopoverButton ref={setAnchor} class='w-full rounded-1'>
				<SidebarItemBase>
					<Plus width={24} height={24} />
					<Text class='truncate font-semibold'>Add feed</Text>
				</SidebarItemBase>
			</PopoverButton>

			<Presence exitBeforeEnter>
				<PopoverPanel
					as={Motion.div}
					ref={setPopper}
					class='w-57 rounded-1 bg-black p-2 shadow-md'
					initial={{ opacity: 0 }}
					animate={{ opacity: 1, transition: { duration: 0.15 } }}
					exit={{ opacity: 0 }}
				>
					<div ref={setArrow} class='-left-1'>
						<div class='h-2 w-2 rotate-45 bg-black' />
					</div>

					<Text h3 class='mb-2'>
						Add Feed
					</Text>
					<Text label class='mb-1 font-semibold'>
						URL
					</Text>
					<form class='mb-1 flex gap-1' onSubmit={onSubmit}>
						<input
							type='text'
							class='w-full rounded-1 bg-black-light p-1'
							value={control.value}
							onInput={(e) =>
								control.setValue(e.currentTarget.value)
							}
							onChange={() => control.markTouched(true)}
						/>
						<button
							class='flex items-center gap-0.5 rounded-1 bg-accent-dark p-1'
							type='submit'
						>
							<Motion.svg
								width={20}
								height={20}
								viewBox='0 0 20 20'
								fill='none'
								xmlns='http://www.w3.org/2000/svg'
								animate={{
									rotate: [0, 360],
								}}
								class={classNames(
									!loading() && 'hidden',
									'p-[2px]'
								)}
								transition={{
									duration: 1,
									repeat: Infinity,
								}}
							>
								<path
									opacity='0.25'
									d='M20 10C20 15.5228 15.5228 20 10 20C4.47715 20 0 15.5228 0 10C0 4.47715 4.47715 0 10 0C15.5228 0 20 4.47715 20 10ZM3 10C3 13.866 6.13401 17 10 17C13.866 17 17 13.866 17 10C17 6.13401 13.866 3 10 3C6.13401 3 3 6.13401 3 10Z'
									fill='#C9CEDA'
								/>
								<path
									d='M17.0711 2.92893C16.1425 2.00035 15.0401 1.26375 13.8268 0.761204C12.6136 0.258657 11.3132 -2.97436e-07 10 0C8.68678 2.97436e-07 7.38642 0.258658 6.17316 0.761206C4.95991 1.26375 3.85752 2.00035 2.92893 2.92893L5.05025 5.05025C5.70026 4.40024 6.47194 3.88463 7.32121 3.53284C8.17049 3.18106 9.08075 3 10 3C10.9193 3 11.8295 3.18106 12.6788 3.53284C13.5281 3.88463 14.2997 4.40024 14.9497 5.05025L17.0711 2.92893Z'
									fill='white'
								/>
							</Motion.svg>
							<Show when={!loading()}>
								<Plus
									width={20}
									height={20}
									class='stroke-white-bright stroke-2'
								/>
							</Show>
							<Text class='font-semibold text-white-bright'>
								Add
							</Text>
						</button>
					</form>
					<Text
						label
						class={classNames(
							control.errors?.message && 'text-[#d34646]',
							'truncate transition-colors'
						)}
					>
						{control.errors?.message || 'Supports RSS 2.0.'}
					</Text>
				</PopoverPanel>
			</Presence>
		</Popover>
	);
};

export default SidebarItemNew;
