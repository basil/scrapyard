import { Motion, Presence } from '@motionone/solid';
import { Link } from 'solid-app-router';
import { Component, createSignal, Show } from 'solid-js';
import type { JSXElement } from 'solid-js';
import Text from '../../Text';
import SidebarItemBase from './SidebarItemBase';
import { RefreshCw } from 'lucide-solid';
import classNames from 'classnames';
import {
	ContextMenu,
	ContextMenuBoundary,
	ContextMenuPanel,
} from 'solid-headless';

type SidebarItemProps = {
	text: string;
	id: string;
	enabled: boolean;
	icon: JSXElement;
	unread?: number;
	reloading?: boolean;
};

const SidebarItemLink: Component<SidebarItemProps> = (props) => {
	const [x, setX] = createSignal(0);
	const [y, setY] = createSignal(0);

	return (
		<ContextMenu class='relative'>
			{({ isOpen, setState }) => {
				return (
					<>
						<ContextMenuBoundary
							onContextMenu={(e: MouseEvent) => {
								if (e.currentTarget) {
									const rect = (
										e.currentTarget as HTMLElement
									).getBoundingClientRect();
									setX(e.clientX - rect.left);
									setY(e.clientY - rect.top);
								}
							}}
						>
							<Link
								href={`/feed/${props.id}`}
								class='block rounded-1'
							>
								<SidebarItemBase enabled={props.enabled}>
									<Show when={props.unread}>
										{/* // TODO: Implement a nice entering animation. Could probably be done via autoAnimate */}
										<p class='z-20 h-2 min-w-2 rounded-full bg-accent-dark px-0.5 text-[0.75rem] font-semibold leading-4 text-white-bright '>
											{props.unread > 99
												? '99+'
												: props.unread}
										</p>
									</Show>
									<div
										class={classNames(
											'z-20 h-3 w-3 min-w-3',
											props.reloading &&
												'flex h-full items-center justify-center rounded-0.5 bg-[#ffffff3a]'
										)}
									>
										{props.reloading ? (
											<Motion
												animate={{ rotate: [0, 360] }}
												transition={{
													repeat: Infinity,
													easing: 'linear',
													duration: 1,
												}}
											>
												<RefreshCw
													width={16}
													height={16}
													class=''
												/>
											</Motion>
										) : (
											props.icon
										)}
									</div>
									<Text class='z-20 truncate font-semibold text-white-bright'>
										{props.text}
									</Text>
									<Presence exitBeforeEnter>
										<Show when={props.enabled}>
											<Motion.div
												class='absolute left-0 right-0 top-0 bottom-0 z-10 py-1'
												animate={{
													left: '0px',
												}}
												initial={{
													left: '-150px',
												}}
												exit={{
													left: '-150px',
												}}
											>
												<div class='w-3 scale-[7.5] opacity-25 blur-sm'>
													{props.icon}
												</div>
											</Motion.div>
										</Show>
									</Presence>
								</SidebarItemBase>
							</Link>
						</ContextMenuBoundary>
						<ContextMenuPanel
							class='absolute z-10'
							style={{
								left: `${x()}px`,
								top: `${y()}px`,
							}}
						>
							<div class='p-0.5 rounded-1 bg-black w-29 flex flex-col justify-start'>
								<button class='py-0.5 px-1 rounded-0.5 bg-accent-dark p-0.5 text-white-bright text-left'>
									Refresh feed
								</button>
								<button class='rounded-0.5 p-0.5 text-white-bright'>
									Refresh feed
								</button>
								<button class='rounded-0.5 p-0.5'>
									Refresh feed
								</button>
								<button class='rounded-0.5 p-0.5'>
									Refresh feed
								</button>
							</div>
						</ContextMenuPanel>
					</>
				);
			}}
		</ContextMenu>
	);
};

export default SidebarItemLink;
