import classNames from 'classnames';
import { JSX, ParentComponent } from 'solid-js';
import { spread } from 'solid-use';

type SidebarItemBaseProps = {
	enabled?: boolean;
} & JSX.ButtonHTMLAttributes<HTMLButtonElement>;

const SidebarItemBase: ParentComponent<SidebarItemBaseProps> = (props) => {
	return (
		<div
			// I'm not sure if it's necessary to destructure it like this via a special function
			{...spread(props)}
			class={classNames(
				props.enabled && 'bg-black',
				'relative flex w-full transform-gpu items-center gap-1 overflow-hidden rounded-1 p-1 transition-colors hover:bg-black'
			)}
		>
			{props.children}
		</div>
	);
};

export default SidebarItemBase;
