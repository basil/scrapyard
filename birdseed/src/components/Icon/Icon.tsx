import classNames from 'classnames';
import type { Component } from 'solid-js';

type IconProps = {
	src: string;
	size?: number;
	class?: string;
};

const Icon: Component<IconProps> = (props) => {
	if (props.size === undefined) props.size = 24;

	return (
		<img
			src={props.src}
			class={classNames(props.class, 'rounded-0.5 bg-black-light outline-none')}
			style={{
				height: `${props.size}px`,
				width: `${props.size}px`,
			}}
		/>
	);
};

export default Icon;
