import { getCurrent } from '@tauri-apps/api/window';
import type { Component } from 'solid-js';

// macOS-styled menu bar. Looking into something platform-agnostic
const MenuBar: Component = () => (
	<div class='h-3 w-screen' data-tauri-drag-region>
		<div class='pointer-events-none h-full w-29 bg-black-dark sm:bottom-1'>
			<div class='group pointer-events-auto flex w-min gap-1 p-1 [&>*:not(:hover)]:border-black-lighter [&:not(:hover)>*]:border-1'>
				<div
					class='pointer-events-auto h-1.5 w-1.5 rounded-1 group-hover:bg-[#EC6A5E]'
					onClick={() => getCurrent().close()}
				/>
				<div
					class='pointer-events-auto h-1.5 w-1.5 rounded-1 group-hover:bg-[#F5BF4F]'
					onClick={() => getCurrent().minimize()}
				/>
				<div
					class='pointer-events-auto h-1.5 w-1.5 rounded-1 group-hover:bg-[#61C554]'
					onClick={async () =>
						getCurrent().setFullscreen(
							!(await getCurrent().isFullscreen())
						)
					}
				/>
			</div>
		</div>
	</div>
);

export default MenuBar;
