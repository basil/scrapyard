import format from 'dateformat';
import { Link, useNavigate, useParams } from 'solid-app-router';
import { Component } from 'solid-js';
import { Post as PostType, setStore, store } from '../../helpers/store';
import ColorBlur from '../ColorBlur';
import Icon from '../Icon';
import Text from '../Text';

const Post: Component = () => {
	const { id: postId, feed: feedId } = useParams<{
		feed: string;
		id: string;
	}>();

	// Navigate back if we can't find a post
	const navigate = useNavigate();
	const feed = store.feeds.find((feed) => feed.id === feedId);
	if (!feed) {
		navigate('/feed/all');
		return null;
	}
	const post: PostType = feed.posts[postId];
	if (!post) {
		navigate(`/feed/${feed.id}`);
		return null;
	}

	// Mark post as read
	if (post.unread)
		setStore('feeds', parseInt(feedId), 'posts', parseInt(postId), {
			...post,
			unread: false,
		});

	return (
		<ColorBlur src={feed.icon}>
			<div class='mb-3 flex select-none items-center gap-1'>
				<Link
					href={`/feed/${feed.id}`}
					class='flex cursor-pointer gap-1 rounded-1 p-0.5 transition-colors hover:bg-black'
				>
					<Icon src={feed.icon} />
					<Text class='px-0.5 font-semibold text-white-bright'>
						{feed.title}
					</Text>
				</Link>
				<svg
					width='10'
					height='24'
					viewBox='0 0 10 24'
					fill='none'
					xmlns='http://www.w3.org/2000/svg'
					class='stroke-black-light'
				>
					<path d='M1 24L9 0' />
				</svg>
				<Text class='px-0.5 font-semibold text-white-bright'>
					{post.title}
				</Text>
			</div>
			<Text h1 class='relative mb-1 flex gap-2'>
				{post.title}
			</Text>
			<div class='mb-3 flex items-center gap-1 text-white-label'>
				<Text class='text-white-bright'>{post.author}</Text>
				<Text>-</Text>
				<Text>{format(post.published, 'mmmm d, yyyy')}</Text>
			</div>
			<article class='article' innerHTML={post.description} />
		</ColorBlur>
	);
};

export default Post;
