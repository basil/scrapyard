import type { ParentComponent } from 'solid-js';

type ColorBlurProps = {
	src: string;
};

const ColorBlur: ParentComponent<ColorBlurProps> = (props) => (
	<>
		<div class='relative z-0 h-0 w-full'>
			<img
				src={props.src}
				class='pointer-events-none absolute left-0 -top-6 z-10 h-12 w-full opacity-10 blur-[400px] transform-gpu rounded-[50%] safari:blur-2xl'
			/>
		</div>

		<div class='relative z-10'>
			{props.children}
		</div>
	</>
);

export default ColorBlur;
