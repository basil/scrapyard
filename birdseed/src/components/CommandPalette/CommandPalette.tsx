import Fuse from 'fuse.js';
import { createFormControl } from 'solid-forms';
import {
	CommandBar,
	CommandBarOverlay,
	CommandBarPanel,
	CommandBarTitle,
} from 'solid-headless';
import {
	Component,
	createMemo,
	createSignal,
	For,
	mergeProps,
	Show,
} from 'solid-js';
import Icon from '../Icon';
import Text from '../Text';

const order = ['feed', 'article', 'command'];

type CommandPaletteProps = {
	actions: Action[];
};
type Category = 'feed' | 'article' | 'command';
type Action = {
	category: Category;
	title: string;
	subtitle?: string;
	icon: string;
	shortcut?: string;
};

// const actionsInput: Action[] = ;

const CommandPalette: Component<CommandPaletteProps> = (props) => {
	const fuse = new Fuse(props.actions, {
		keys: ['title', 'subtitle'],
	});

	const [open, setOpen] = createSignal(false);
	// const [selected]
	const control = createFormControl('');

	const results = createMemo(() =>
		control.value
			? fuse.search(control.value).map((item) => item.item)
			: props.actions
	);

	const options = createMemo<Record<string, Action[]>>(() =>
		results().reduce(
			(prev: any, action: Action) =>
				Object.assign(prev, {
					[action.category]: [
						...(prev[action.category] || []),
						action,
					],
				}),
			{}
		)
	);

	return (
		<CommandBar
			isOpen={open()}
			onOpen={() => setOpen(true)}
			onClose={() => setOpen(false)}
			class='fixed inset-0 z-50 overflow-y-auto'
		>
			<CommandBarOverlay class='fixed inset-0 bg-black-darker/50' />
			<CommandBarPanel class='fixed inset-0 m-auto flex h-42 w-87 flex-col rounded-1 bg-black-dark'>
				<input
					type='text'
					class='w-full border-0 bg-transparent p-2 text-white-bright placeholder:text-white-label focus:ring-0'
					placeholder='Search for actions...'
					value={control.value}
					onInput={(e) => control.setValue(e.currentTarget.value)}
				/>
				<hr class='border-black' />
				<div class='m-1 overflow-y-auto'>
					<For
						each={Object.keys(options()).sort(
							(a, b) => order.indexOf(a) - order.indexOf(b)
						)}
					>
						{(key) => (
							<>
								<Text
									label
									class='p-1 font-semibold text-white-label'
								>
									{key.replace(/^\w/, (c) => c.toUpperCase())}
								</Text>
								<For each={options()[key]}>
									{(action) => (
										<div class='flex justify-between rounded-1 p-1'>
											<div class='flex gap-1'>
												<Icon src={action.icon} />
												<Text class='text-white-bright'>
													{action.title}
												</Text>
												<Show when={action.subtitle}>
													<Text class='text-white-label'>
														{action.subtitle}
													</Text>
												</Show>
											</div>
											<div class='flex gap-0.5'>
												{/* <Show when={action.shortcut}>

												</Show>
												<kbd class='h-3 w-3 rounded-0.5 bg-black-light text-center font-body text-white-label'>
													F
												</kbd>
												<kbd class='h-3 w-3 rounded-0.5 bg-black-light text-center font-body text-white-label'>
													1
												</kbd> */}
											</div>
										</div>
									)}
								</For>
							</>
						)}
					</For>
				</div>

				{/*<div class='m-1 overflow-y-auto'>
					<For
						each={Object.keys(actions()).sort(
							(a, b) => order.indexOf(a) - order.indexOf(b)
						)}
					>
						{(key) => (
							<>
								<Text
									label
									class='p-1 font-semibold text-white-label'
								>
									{key.replace(/^\w/, (c) => c.toUpperCase())}
								</Text>
								<For each={actions()[key]}>
									{(action) => (
										<div class='flex justify-between rounded-1 p-1'>
											<div class='flex gap-1'>
												<Icon src={action.icon} />
												<Text class='text-white-bright'>
													{action.title}
												</Text>
												<Show when={action.subtitle}>
													<Text class='text-white-label'>
														{action.subtitle}
													</Text>
												</Show>
											</div>
											<div class='flex gap-0.5'>
												{/* <Show when={action.shortcut}>

												</Show>
												<kbd class='h-3 w-3 rounded-0.5 bg-black-light text-center font-body text-white-label'>
													F
												</kbd>
												<kbd class='h-3 w-3 rounded-0.5 bg-black-light text-center font-body text-white-label'>
													1
												</kbd> *}
											</div>
										</div>
									)}
								</For>
							</>
									)}
					</For>
				</div>*/}
			</CommandBarPanel>
		</CommandBar>
	);
};

export default CommandPalette;
