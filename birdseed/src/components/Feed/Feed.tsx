import { useNavigate, useParams } from 'solid-app-router';
import { Component, createMemo } from 'solid-js';
import { useStore } from '../../helpers/store';
import FeedBuilder from './FeedBuilder';

const Feed: Component = () => {
	const params = useParams<{ id: string }>();

	const feed = createMemo(() =>
		useStore().feeds.find((store) => store.id === params.id)
	);

	// If we can't find the feed, go back to all feeds
	const navigate = useNavigate();
	if (!feed()) {
		navigate('/feed/all');
		return null;
	}

	return (
			<FeedBuilder
				posts={feed().posts}
				title={feed().title}
				icon={feed().icon}
				feedId={feed().id}
			/>
	);
};

export default Feed;
