import { Link } from 'solid-app-router';
import { Component, For } from 'solid-js';
import type { JSXElement } from 'solid-js';
import { Feed, Post, useStore } from '../../helpers/store';
import ColorBlur from '../ColorBlur';
import Icon from '../Icon';
import Text from '../Text';
import format from 'dateformat';

type PostWithId = Post & {
	feedId?: string;
};

type FeedBuilderProps = {
	// feed: Accessor<Feed>;
	icon?: string | JSXElement;
	title: string;
	feedId?: string;
	posts: PostWithId[];
};

const FeedBuilder: Component<FeedBuilderProps> = (props) => {
	// Is this good practice?
	let feeds: Record<string, Feed> = {};
	const getFeed = (id: string) => {
		if (feeds[id]) return feeds[id];

		const feed = useStore().feeds.find((feed) => feed.id === id);
		if (feed) {
			feeds[id] = feed;
			return feed;
		}
		return undefined;
	};

	return (
		<ColorBlur src={typeof props.icon === 'string' ? props.icon : ''}>
			<Text h1 class='mb-3 flex gap-2'>
				{typeof props.icon === 'string' ? (
					<Icon src={props.icon} size={48} />
				) : (
					<>{props.icon}</>
				)}
				{props.title}
			</Text>

			<div class='flex flex-col gap-1'>
				<For each={props.posts}>
					{(post, i) => (
							<Link
								href={`/feed/${
									props.feedId || post.feedId
								}/${i()}`}
								class='flex flex-col gap-1 rounded-1 bg-black-dark p-2 transition-colors hover:bg-black'
							>
								<Text h2 class='mb-1 text-left'>
									{post.title}
								</Text>
								{(post.author ||
									post.published ||
									post.unread) && (
									<span class='flex gap-1 text-white-label'>
										{post.unread && (
											<Text
												label
												class='rounded-2 bg-accent-dark px-1 py-0.5 font-semibold text-white-bright'
											>
												New
											</Text>
										)}
										{[
											post.feedId && (
												<Text class='flex gap-1 font-semibold text-white-bright'>
													<Icon
														src={
															getFeed(post.feedId)
																.icon
														}
														class='inline'
													/>
													{getFeed(post.feedId).title}
												</Text>
											),
											post.author && (
												<Text class='font-semibold text-white-bright'>
													{post.author}
												</Text>
											),
											post.published && (
												<Text class='text-white-label'>
													{format(
														post.published,
														'mmmm d, yyyy'
													)}
												</Text>
											),
										]
											.filter((e) => e)
											.reduce(
												(prev: JSXElement[], el) => [
													...prev,
													prev.length ? (
														<Text class='text-white-label'>
															-
														</Text>
													) : null,
													el,
												],
												[]
											)}
									</span>
								)}
							</Link>
					)}
				</For>
			</div>
		</ColorBlur>
	);
};

export default FeedBuilder;
export type { PostWithId };
