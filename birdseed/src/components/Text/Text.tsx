import classNames from 'classnames';
import type { ParentComponent } from 'solid-js';

type TextProps = {
	h1?: true;
	h2?: true;
	h3?: true;
	label?: true;
	class?: string;
};

const Text: ParentComponent<TextProps> = (props) => {
	if (props.h1)
		return <h1 class={classNames('text-h1 font-bold text-white-bright', props.class)}>{props.children}</h1>;
	else if (props.h2)
		return <h2 class={classNames('text-h2 font-bold text-white-bright', props.class)}>{props.children}</h2>;
	else if (props.h3)
		return <h3 class={classNames('text-h3 font-bold text-white-bright', props.class)}>{props.children}</h3>;
	else if (props.label)
		return <p class={classNames('text-sm', props.class)}>{props.children}</p>;
	else return <p class={classNames(props.class)}>{props.children}</p>;
};

export default Text;
