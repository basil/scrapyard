import { Component, createMemo } from 'solid-js';
import { useStore } from '../../helpers/store';
import FeedBuilder from '../Feed/FeedBuilder';
import type { PostWithId } from '../Feed/FeedBuilder';
import { Inbox } from 'lucide-solid';

const FeedAll: Component = () => {
	const feeds = createMemo<PostWithId[]>(() =>
		useStore()
			.feeds.map<PostWithId[]>((feed) =>
				feed.posts.map((post) => ({ ...post, feedId: feed.id }))
			)
			.reduce<PostWithId[]>((prev, feed) => [...prev, ...feed], [])
			.sort(
				(a, b) =>
					(b.published ? new Date(b.published).getTime() : 0) -
					(a.published ? new Date(a.published).getTime() : 0)
			)
	);

	return <FeedBuilder posts={feeds()} title='All' icon={<Inbox width={48} height={48} />} />;
};

export default FeedAll;
