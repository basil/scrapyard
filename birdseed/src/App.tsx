import type { Component } from 'solid-js';
import Inspect from './components/Inspect';
import Sidebar from './components/Sidebar';
import { Navigate, Route, Routes } from 'solid-app-router';
import { StoreProvider } from './helpers/store';
import Feed from './components/Feed';
import Post from './components/Post';
import FeedAll from './components/FeedAll';
import CommandPalette from './components/CommandPalette';

const App: Component = () => (
	<StoreProvider>
		<Inspect>
			<CommandPalette
				actions={[
					{
						category: 'feed',
						title: 'Hundred Rabbits',
						subtitle: 'Some Article',
						icon: 'https://100r.co/media/services/rss.jpg',
						shortcut: 'shift+1',
					},
					{
						category: 'command',
						title: 'Refresh feed',
						icon: 'https://grimgrains.com/media/services/rss.jpg',
						shortcut: 'r',
					},
					{
						category: 'feed',
						title: 'Grimgrains',
						icon: 'https://grimgrains.com/media/services/rss.jpg',
						shortcut: 'shift+2',
					},
					{
						category: 'feed',
						title: 'Grimgrains',
						icon: 'https://grimgrains.com/media/services/rss.jpg',
						shortcut: 'shift+3',
					},
					{
						category: 'feed',
						title: 'Grimgrains',
						icon: 'https://grimgrains.com/media/services/rss.jpg',
						shortcut: 'shift+4',
					},
					{
						category: 'article',
						title: 'Summary',
						subtitle: 'Hundred Rabbits',
						icon: 'https://grimgrains.com/media/services/rss.jpg',
						shortcut: '1',
					},
					{
						category: 'feed',
						title: 'Grimgrains',
						icon: 'https://grimgrains.com/media/services/rss.jpg',
						shortcut: 'shift+5',
					},
					{
						category: 'article',
						title: 'corn pone',
						subtitle: 'Grimgrains',
						icon: 'https://grimgrains.com/media/services/rss.jpg',
						shortcut: '2',
					},
				]}
			/>
			<div class='flex h-screen flex-col overflow-hidden bg-black-darker text-white'>
				{/* <MenuBar /> */}
				<div class='flex h-full'>
					<Sidebar />
					<div class='h-auto w-full overflow-y-auto bg-black-darker px-6'>
						<main class='m-auto mt-3 mb-12 max-w-87'>
							<Routes>
								<Route path='/feed/all' element={<FeedAll />} />
								<Route path='/feed/:id' element={<Feed />} />
								<Route
									path='/feed/:feed/:id'
									element={<Post />}
								/>
								<Route
									path='*'
									element={<Navigate href='/feed/all' />}
								/>
							</Routes>
						</main>
					</div>
				</div>
			</div>
		</Inspect>
	</StoreProvider>
);

export default App;
