const plugin = require('tailwindcss/plugin');

/** @type {import('tailwindcss').Config} */
module.exports = {
	darkMode: 'class',
	minify: true,
	content: ['./src/**/*.tsx', './*.html'],
	theme: {
		extend: {
			minWidth: {
				// Sidebar
				29: '232px',
				2: '16px',
				3: '24px',
			},
			maxWidth: {
				87: '696px',
			},
			borderWidth: {
				1: '1px',
			},
		},
		colors: {
			transparent: 'transparent',
			current: 'currentColor',
			white: {
				bright: '#FFFFFF',
				DEFAULT: '#C9CEDA',
				label: '#989BA3',
			},
			black: {
				lighter: '#3E3E42',
				light: '#303036',
				DEFAULT: '#24252B',
				dark: '#17181F',
				darker: '#0A0B0F',
			},
			accent: {
				DEFAULT: '#7197FF',
				dark: '#3167FB',
			},
		},
		fontSize: {
			h1: ['2.625rem', '3rem'],
			h2: ['1.625rem', '2rem'],
			h3: ['1.25rem', '1.5rem'],
			base: ['1rem', '1.5rem'],
			sm: ['0.875rem', '1rem'],
		},
		fontWeight: {
			normal: 400,
			semibold: 600,
			bold: 700,
		},
		fontFamily: {
			body: ['"Inter"', 'sans-serif'],
		},
		spacing: {
			0: '0px',
			0.5: '4px',
			1: '8px',
			1.5: '12px',
			2: '16px',
			3: '24px',
			6: '48px',
			// Sidebar
			29: '232px',
			// 1 column
			12: '96px',
			// 3 columns
			42: '336px',
			// 4 columns
			57: '456px',
			// 6 columns
			87: '696px',
		},
		borderRadius: {
			0.5: '4px',
			1: '8px',
			2: '16px',
			full: '9999px',
		},
	},
	plugins: [
		plugin(({ addVariant }) => {
			addVariant(
				'safari',
				'@media not all and (min-resolution:.001dpcm)'
			);
		}),
		require('@tailwindcss/forms'),
	],
};
