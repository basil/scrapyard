const plugin = require('tailwindcss/plugin');

/** @type {import('tailwindcss').Config} */
module.exports = {
	content: ['./index.html', './src/**/*.tsx'],
	theme: {
		extend: {},
		fontFamily: {
			sans: ['"Teko"', 'sans-serif'],
		},
		fontSize: {
			sm: ['1rem', '1.5rem'],
			base: ['1.5rem', '2rem'],
			lg: ['2.625rem', '3rem'],
			xl: ['4.25rem', '4.5rem'],
		},
		colors: {
			bg: '#0E253C',
			'bg-alt': '#092036',
			// Actually lighter, but is dark for consistency reasons. Could be changed to "hover"
			'bg-alt-dark': '#133353',
			white: '#FFFFFF',
			'white-alt': '#848F9B',
			'white-alt-dark': '#6E7C8A',
			blue: '#0b86af',
			'blue-dark': '#2F759B',
			green: '#00BC8C',
			'green-dark': '#00A87D',
			red: '#F05E5E',
			'red-dark': '#DB4E4E',
			transparent: 'transparent',
		},
		spacing: {
			0: '0px',
			0.5: '4px',
			1: '8px',
			1.5: '12px',
			2: '16px',
			3: '24px',
			4: '32px',
			5: '40px',
		},
		maxWidth: {
			page: '936px',
		},
		borderRadius: {
			0.5: '4px',
			1: '8px',
		},
	},
	plugins: [
		plugin(({ addVariant }) => {
			addVariant('child', '&>*');
		}),
		require('@tailwindcss/forms'),
	],
};
