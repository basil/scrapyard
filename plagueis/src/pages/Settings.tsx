import { Component } from 'solid-js';
import { open } from '@tauri-apps/api/dialog';
import Button from '../components/Button';
import Header from '../components/Header';
import TextInput from '../components/TextInput';

const Settings: Component = () => {
	return (
		<>
			<Header title='Settings' />
			<div class='mt-3 text-sm'>
				<h3>Install Folder</h3>
				<div class='flex flex-row gap-1 mt-1'>
					<TextInput placeholder='Path to folder' class='flex-1 text-sm' />
					<Button onClick={async () => {
						const folder = await open({
							directory: true
						});
						console.log(folder);
					}}>Select Folder</Button>
				</div>
			</div>
		</>
	);
};

export default Settings;
