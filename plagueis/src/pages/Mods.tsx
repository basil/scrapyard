import { Component, createMemo, createSignal, For, Show } from 'solid-js';
import Header from '../components/Header';
import { Filter, Search } from '../components/Icon';
import Mod from '../components/Mod';
import getMods from '../helpers/getMods';
import sortMods from '../helpers/sortMods';
import InstalledMod from '../types/core/InstalledMod';
import Fuse from 'fuse.js';
import ModState from '../types/core/ModState';
import { setState, state } from '../helpers/useStore';

enum Status {
	Success,
	Fetching,
	Error,
	NoResults,
}

const Mods: Component = () => {
	const [status, setStatus] = createSignal<Status>(Status.Fetching);

	// The mods shown on the mod page (affected by search)
	const [modList, setModList] = createSignal<InstalledMod[]>([]);
	// Fuzzy search
	const search = createMemo<Fuse<InstalledMod>>(
		() =>
			new Fuse(state.mods, {
				keys: ['name'],
			})
	);

	getMods()
		.then((mods) => {
			// The list of all mods
			setState(
				'mods',
				mods.map<InstalledMod>((mod) => ({
					...mod,
					// TODO: Actually reflect installed state
					state: ModState.Uninstalled,
				}))
			);
			setModList(state.mods);

			setStatus(Status.Success);
		})
		.catch(() => {
			setStatus(Status.Error);
			// toast('( >︿< ) Failed to fetch mods!');
		});

	return (
		<>
			<Header title='Mods' />
			<div class='mt-3 flex gap-1'>
				<div class='relative w-full rounded-1 bg-bg-alt'>
					<Search class='pointer-events-none absolute left-2 top-1 bottom-1 h-4 w-4 stroke-white-alt' />
					<input
						class='w-full rounded-1 bg-transparent py-1 pl-[64px] placeholder:text-white-alt'
						// Don't allow the user to search if there aren't any results.
						disabled={!state.mods.length}
						placeholder='Search'
						onInput={(e) => {
							const value = (e.target as HTMLInputElement).value;

							// If not searching, reset
							if (value === '') {
								setModList(state.mods);
								setStatus(Status.Success);
								return;
							}

							const results = search()
								.search((e.target as HTMLInputElement).value)
								.map((result) => result.item);

							// Show "No Results" message
							if (!results.length) setStatus(Status.NoResults);
							else if (results.length && status() === Status.NoResults)
								setStatus(Status.Success);

							setModList(results);
						}}
					/>
				</div>
				{/* <button class='rounded-1 bg-bg-alt px-3 py-1'>
					<Filter class='h-4 w-4 stroke-white' />
				</button> */}
			</div>

			<div class='mt-3'>
				<Show when={status() === Status.Fetching}>
					<p class='text-center'>Fetching mods...</p>
				</Show>
				<Show when={status() === Status.Error}>
					<p class='text-center text-red'>{'( >︿< )'} Failed to fetch mods!</p>
				</Show>
				<Show when={status() === Status.NoResults}>
					<p class='text-center'>No results</p>
				</Show>
				<Show when={status() === Status.Success}>
					<For
						each={sortMods(modList())}
						fallback={<p class='text-center'>Loading mods...</p>}
					>
						{(category) => (
							<>
								<h2 class='mb-1 inline-block rounded-1 bg-bg-alt px-4 py-0.5'>
									{category.title}
								</h2>
								<div class='mb-5 flex flex-col gap-1'>
									<For each={category.mods}>{(mod) => <Mod mod={mod} />}</For>
								</div>
							</>
						)}
					</For>
				</Show>
			</div>
		</>
	);
};

export default Mods;
