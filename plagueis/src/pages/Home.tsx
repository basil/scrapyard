import type { Component } from 'solid-js';
import { Cog, Songs, Syringe } from '../components/Icon';
import MenuButton from '../components/MenuButton';

const Home: Component = () => {
	return (
		<>
			<h1 class='text-center text-xl'>Plagueis</h1>
			<div class='flex w-full flex-row gap-3 mt-3'>
				<MenuButton icon={<Syringe />} title='Mods' href='/mods' />
				{/* <MenuButton icon={<Songs />} title='Songs' href='/songs' /> */}
				<MenuButton icon={<Cog />} title='Settings' href='/settings' />
			</div>
		</>
	);
};

export default Home;
