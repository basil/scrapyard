import type { Component } from 'solid-js';

const Spinner: Component = () => {
	return (
		<div class='h-2 w-2 animate-spin rounded-1 border-2 border-transparent border-r-white' />
	);
};

export default Spinner;
