import type { Component, JSX } from 'solid-js';
import { Link } from '@solidjs/router';

type MenuButtonProps = {
	title: string;
	href: string;
	icon: JSX.Element;
};

const MenuButton: Component<MenuButtonProps> = (props) => {
	return (
		<Link href={props.href} class='w-full'>
			<div class='group relative flex flex-col items-center gap-2 rounded-[6px] bg-bg-alt py-2 transition-colors hover:bg-blue'>
				<h2>{props.title}</h2>
				{/* {<>{props.icon}</>} */}
				<div class='child:h-[96px] child:w-[96px] child:stroke-white-alt child:transition-colors child:group-hover:stroke-white'>
					{props.icon}
				</div>
				{/* <Syringe class='h-[96px] w-[96px] stroke-white-alt transition-colors group-hover:stroke-white' /> */}
				{/* <div class='arounded-1 absolute inset-[-2px] -z-10 rounded-1 bg-opacity-20 bg-gradient-to-b from-[transparent] to-white-alt p-[2px]'></div> */}
			</div>
		</Link>
	);
};

export default MenuButton;
