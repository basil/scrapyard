import type { Component, JSX } from 'solid-js';

type TextInputProps = JSX.InputHTMLAttributes<HTMLInputElement>;

const TextInput: Component<TextInputProps> = (props) => {
	return (
		<input
			type='text'
			class={
				`rounded-1 border-none bg-bg-alt py-1 px-1.5 text-base transition-colors ` +
				(props.class || '')
			}
			{...props}
		/>
	);
};

export default TextInput;
