import { Link } from '@solidjs/router';
import type { Component } from 'solid-js';
import { ChevronLeft } from '../Icon';

type HeaderProps = {
	title: string;
};

const Header: Component<HeaderProps> = ({ title }) => {
	return (
		<nav class='flex -skew-x-6 gap-1'>
			<Link href='/'>
				<div class='w-min rounded-1 bg-blue py-1 px-3 transition-colors hover:bg-blue-dark'>
					<ChevronLeft class='h-5 w-5 stroke-white' />
				</div>
			</Link>
			<div class='w-full rounded-1 bg-gradient-to-r from-blue to-transparent py-1'>
				<h1 class='h-5 cursor-default select-none text-center text-lg'>
					{title}
				</h1>
			</div>
		</nav>
	);
};

export default Header;
