import Syringe from './Syringe';
import ChevronLeft from './ChevronLeft';
import Search from './Search';
import Filter from './Filter';
import ArrowRight from './ArrowRight';
import Download from './Download';
import Info from './Info';
import Trash2 from './Trash2';
import Songs from './Songs';
import Cog from './Cog';

export { Syringe, ChevronLeft, Search, Filter, ArrowRight, Download, Info, Trash2, Songs, Cog };
