import type { Component, JSX } from 'solid-js';

type SongsProps = JSX.SvgSVGAttributes<SVGSVGElement>;

const Songs: Component<SongsProps> = (props) => {
	return (
		<svg
			xmlns='http://www.w3.org/2000/svg'
			width='24'
			height='24'
			viewBox='0 0 24 24'
			fill='none'
			stroke='#F56565'
			stroke-width='2'
			stroke-linecap='round'
			stroke-linejoin='round'
			{...props}
		>
			<path d='M9 18V5l12-2v13' />
			<circle cx='6' cy='18' r='3' />
			<circle cx='18' cy='16' r='3' />
		</svg>
	);
};

export default Songs;
