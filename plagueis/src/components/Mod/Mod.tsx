import { Component, Show } from 'solid-js';
import InstalledMod from '../../types/core/InstalledMod';
import lt from 'semver/functions/lt';
import { ArrowRight, Download, Info, Trash2 } from '../Icon';
import { open } from '@tauri-apps/api/shell';
import isModUpdatable from '../../helpers/isModUpdatable';
import { state, setState } from '../../helpers/useStore';
import ModState from '../../types/core/ModState';
import installMod from '../../helpers/installMod';
import uninstallMod from '../../helpers/uninstallMod';

type ModProps = {
	mod: InstalledMod;
};

const Mod: Component<ModProps> = ({ mod }) => {
	const setMod = (obj: Partial<InstalledMod>) =>
		setState('mods', state.mods.indexOf(mod), {
			...mod,
			...obj,
		});

	return (
		<div class='rounded-1 bg-bg-alt p-0.5 pl-1'>
			<div class='row flex justify-between'>
				{/* Title and version */}
				<span>
					<h3 class='inline'>{mod.name}</h3>

					<Show
						when={isModUpdatable(mod)}
						fallback={<a class='ml-2 text-sm'>{mod.version}</a>}
					>
						<div class='row ml-2 inline-flex items-center gap-0.5'>
							<a class='text-sm text-red'>{mod.installedVersion}</a>
							<ArrowRight class='h-2 w-2 stroke-white-alt' />
							<a class='text-sm'>{mod.version}</a>
						</div>
					</Show>
				</span>

				{/* Action buttons */}
				<div class='row flex h-fit gap-1'>
					{/* Update if available button */}
					<Show
						when={mod.installedVersion && lt(mod.installedVersion, mod.version)}
					>
						<button class='rounded-0.5 bg-green p-0.5 transition-colors hover:bg-green-dark'>
							<Download class='h-2 w-2 stroke-white' />
						</button>
					</Show>

					{/* Info button */}
					<button
						class='rounded-0.5 bg-white-alt p-0.5 transition-colors hover:bg-white-alt-dark'
						onClick={() => open(mod.link)}
					>
						<Info class='h-2 w-2 stroke-white' />
					</button>

					{/* Download button */}
					<Show
						when={
							mod.state === ModState.Uninstalled ||
							mod.state === ModState.Installing
						}
					>
						<button
							class='rounded-0.5 bg-blue p-0.5 transition-colors hover:bg-blue-dark disabled:bg-blue-dark'
							onClick={() => {
								setMod({ state: ModState.Installing });

								installMod(mod)
									.then(() =>
										setMod({
											state: ModState.Installed,
											installedVersion: mod.version,
										})
									)
									.catch(() => setMod({ state: ModState.Uninstalled }));
							}}
							// Disable the button if the mod is currently undergoing an action
							disabled={mod.state === ModState.Installing}
						>
							{/* Either show the download button or the spinner */}
							<Show
								when={mod.state === ModState.Installing}
								fallback={<Download class='h-2 w-2 stroke-white' />}
							>
								<div class='h-2 w-2 animate-spin rounded-[100%] border-2 border-transparent border-r-white' />
							</Show>
						</button>
					</Show>

					{/* Uninstall button */}
					<Show
						when={
							mod.state === ModState.Installed ||
							mod.state === ModState.Uninstalling
						}
					>
						<button
							class='rounded-0.5 bg-red p-0.5 transition-colors hover:bg-red-dark disabled:bg-red-dark'
							onClick={() => {
								setMod({ state: ModState.Uninstalling });

								uninstallMod(mod)
									.then((mod) =>
										// TODO: remove installVersion key
										setMod(mod)
									)
									.catch(() => setMod({ state: ModState.Uninstalled }));
							}}
							// Disable the button if the mod is currently undergoing an action
							disabled={mod.state === ModState.Uninstalling}
						>
							{/* Either show the download button or the spinner */}
							<Show
								when={mod.state === ModState.Uninstalling}
								fallback={<Trash2 class='h-2 w-2 stroke-white' />}
							>
								<div class='h-2 w-2 animate-spin rounded-[100%] border-2 border-transparent border-r-white' />
							</Show>
						</button>
					</Show>
				</div>
			</div>

			<p class='text-sm text-white-alt'>{mod.description}</p>
		</div>
	);
};

export default Mod;
