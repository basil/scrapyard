import type { Component, JSX } from 'solid-js';

type ButtonInputProps = JSX.ButtonHTMLAttributes<HTMLButtonElement>;

const ButtonInput: Component<ButtonInputProps> = (props) => {
	return (
		<button
			class={
				`Button-base rounded-1 border-none bg-blue py-1 px-1.5 transition-colors hover:bg-blue-dark ` +
				(props.class || '')
			}
			{...props}
		>
			{props.children}
		</button>
	);
};

export default ButtonInput;
