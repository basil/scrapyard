enum ModState {
	Uninstalled,
	Installed,
	Installing,
	Uninstalling,
}

export default ModState;
