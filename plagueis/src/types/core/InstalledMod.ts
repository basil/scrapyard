import Mod from '../beatmods/Mod';
import ModState from './ModState';

type InstalledMod = Mod & {
	state: ModState;
	installedVersion?: string;
};

export default InstalledMod;
