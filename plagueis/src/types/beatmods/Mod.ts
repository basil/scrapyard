// The BeatMods API doesn't seem to be documented.
type Mod = {
	name: string;
	version: string;
	gameVersion: string;
	authorId: string;
	uploadDate: string;
	updatedDate: string;
	author?: { // Doesn't seem to exist when nested in a mod
		_id: string;
		username: string;
		lastLogin: string;
	};
	status: string;
	description: string;
	link: string;
	category: string;
	downloads: {
		type: 'universal'; // None of this appears to be documented, there may be other download types that I don't know of
		url: string;
		hashMd5: {
			hash: string;
			file: string;
		}[];
	}[];
	required: boolean;
	dependencies: Mod[] | string[]; // Seems to be Mod[] when not nested in a mod and string[] when nested in a mod
	_id: string;
};

export default Mod;
