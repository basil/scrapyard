import { createSignal } from 'solid-js';
import { invoke } from '@tauri-apps/api/tauri';

function App() {
	const [greetMsg, setGreetMsg] = createSignal('');
	const [name, setName] = createSignal('');

	async function greet() {
		setGreetMsg(await invoke('greet', { name: name() }));
	}

	return (
		<div class='container'>
			<h1>Welcome to Tauri!</h1>

			<div class='row h-6 bg-bg-alt'>
			</div>

			<p>
				Click on the Tauri, Vite, and Solid logos to learn more about each
				framework.
			</p>

			<div class='row'>
				<div>
					<input
						id='greet-input'
						onChange={(e) => setName(e.currentTarget.value)}
						placeholder='Enter a name...'
					/>
					<button type='button' onClick={() => greet()}>
						Greet
					</button>
				</div>
			</div>

			<p>{greetMsg}</p>
		</div>
	);
}

export default App;
