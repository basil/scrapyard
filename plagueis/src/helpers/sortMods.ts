import InstalledMod from '../types/core/InstalledMod';

// Takes an array of mods and returns an array of an array of mods (Mod[][]) sorted
// alphabetically by category, and optionally hoist the mods with available updates
// to the top. Not a oneliner for readability purposes. Could be more efficient
// though.
const sortMods = (
	mods: InstalledMod[],
	hoistUpdatable: boolean = false
): { title: string; mods: InstalledMod[] }[] => {
	// return mods.

	// Get all unique categories
	const categories: string[] = mods
		.map((mod) => mod.category)
		.filter((category, index, self) => self.indexOf(category) === index)
		.sort();

	// Make an array with an empty array for each category
	let sorted: { title: string; mods: InstalledMod[] }[] = Array(
		categories.length
	)
		.fill(null)
		.map((_, i) => ({ title: categories[i], mods: [] }));

	// Put each mod into the right category
	mods.forEach((mod) => sorted[categories.indexOf(mod.category)].mods.push(mod));

	// Sort each category by title
	sorted.forEach((arr) =>
		arr.mods.sort((a, b) => {
			var textA = a.name.toUpperCase();
			var textB = b.name.toUpperCase();
			return textA < textB ? -1 : textA > textB ? 1 : 0;
		})
	);

	return sorted;

	// if (hoistUpdatable && mods.some(mod => isModUpdatable(mod))) {
	// 	categories.unshift('Updatable');
	// }
};

export default sortMods;
