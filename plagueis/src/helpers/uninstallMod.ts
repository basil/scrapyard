import Mod from '../types/beatmods/Mod';
import InstalledMod from '../types/core/InstalledMod';
import ModState from '../types/core/ModState';

const uninstallMod = (input: InstalledMod) =>
	new Promise<Mod>((resolve, reject) => {
		const mod = Object.assign({}, input);

		mod.state = ModState.Uninstalled;
		delete mod.installedVersion;

		// TODO: Implement logic

		resolve(mod);
	});

export default uninstallMod;
