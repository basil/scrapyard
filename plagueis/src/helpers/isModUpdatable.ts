import lt from "semver/functions/lt";
import InstalledMod from "../types/core/InstalledMod";

const isModUpdatable = (mod: InstalledMod) => {
	mod.installedVersion && lt(mod.installedVersion, mod.version)
};

export default isModUpdatable;
