import Mod from '../types/beatmods/Mod';
import InstalledMod from '../types/core/InstalledMod';
import ModState from '../types/core/ModState';

const installMod = (input: Mod) =>
	new Promise<InstalledMod>((resolve, reject) => {
		const mod = Object.assign({}, input) as InstalledMod;

		mod.state = ModState.Installed;
		mod.installedVersion = mod.version;

		// TODO: Implement logic

		resolve(mod);
	});

export default installMod;
