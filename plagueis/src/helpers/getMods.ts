import { http } from '@tauri-apps/api';
import Mod from '../types/beatmods/Mod';

const getMods = async () =>
	(
		await http.fetch(
			'https://beatmods.com/api/v1/mod?status=approved&gameVersion=1.21.0'
		)
	).data as Mod[];

export default getMods;
