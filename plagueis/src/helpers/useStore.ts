import { createStore } from 'solid-js/store';
import InstalledMod from '../types/core/InstalledMod';

type Store = {
	mods: InstalledMod[];
};

const [state, setState] = createStore<Store>({
	mods: []
});

export { state, setState };
