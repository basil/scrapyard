import { Route, Routes } from '@solidjs/router';
import type { Component } from 'solid-js';
import Home from './pages/Home';
import Mods from './pages/Mods';
import Settings from './pages/Settings';

const App: Component = () => {
	return (
		<Routes>
			<Route path='/' component={Home} />
			<Route path='/mods' component={Mods} />
			<Route path='/settings' component={Settings} />
		</Routes>
	);
};

export default App;
