# solid-inspx

[inspx](https://github.com/raunofreiberg/inspx) but for Solid.

## Credits

- https://github.com/raunofreiberg/inspx for the meat and potatoes
- https://github.com/radix-ui/primitives for the popper script. I had to extract it to a file here due to Vite issues.