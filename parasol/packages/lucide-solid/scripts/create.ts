import { readdir, readFile, writeFile } from 'fs/promises';
import { cwd } from 'process';
import camelcase from 'camelcase';
import { join } from 'path';

const staticIconsPath = join(cwd(), '../../node_modules/lucide-static/icons');
const solidIconsPath = cwd() + '/src/icons';
const barrelFile = cwd() + '/src/index.ts';
const toSolid = (name: string, svgContent: string) =>
	`
import { SVGProps } from "../types"
export const ${name} = (props: SVGProps) => (${svgContent.replace(
		'>',
		'{...props}>'
	)})
`.trim();

let toIcon = (svgContent: string) =>
	svgContent
		.replace(/width="\d+"/, 'width="1em"')
		.replace(/height="\d+"/, 'height="1em"');

async function main() {
	let files = await readdir(staticIconsPath);
	let icons: Promise<void>[] = [];
	let barrel = '';

	for (let file of files) {
		let content = await readFile(staticIconsPath + '/' + file, {
			encoding: 'utf-8',
		});
		// Custom path to allow for the parasol design to work properly.
		content = content
			.replace(
				/<(path|line|rect|circle|polyline) /g,
				(_, el) => `<${el} vector-effect="non-scaling-stroke"`
			)
			.replace(/stroke-width="2"/g, 'stroke-width="1"');
		let name = file.slice(0, file.lastIndexOf('.'));

		let comp = toSolid(
			camelcase(name, { pascalCase: true }),
			toIcon(content)
		);

		icons.push(
			writeFile(`${solidIconsPath}/${name}.tsx`, comp, {
				encoding: 'utf-8',
			})
		);

		barrel += `export { ${camelcase(name, {
			pascalCase: true,
		})} } from './icons/${name}';\n`;
	}

	icons.push(writeFile(barrelFile, barrel, { encoding: 'utf-8' }));

	await Promise.all(icons);
}

main();
