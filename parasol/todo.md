# TODO

- [ ] Handle password in Pi-hole
- [ ] Test Pi-hole
- [ ] Make widgets importable from an external source, like apps.
- [ ] Route traffic to domain (cloudflared)
- [ ] Theming
