import { readFile, writeFile, mkdir } from 'fs/promises';
import { existsSync } from 'fs';
import { join } from 'path';

const get = async <Type>(file: string, defaultValue: Type): Promise<Type> => {
	try {
		const path = join('config', file);
		const data = await readFile(path, 'utf8');
		return JSON.parse(data);
	} catch {
		set(file, defaultValue);
		return defaultValue;
	}
};

const set = async <Type>(file: string, value: Type) => {
	try {
		const path = join('config', file);
		if (!existsSync('config')) await mkdir('config');
		await writeFile(path, JSON.stringify(value, null, '\t'));
	} catch {
		return;
	}
	return;
};

export { get, set };
