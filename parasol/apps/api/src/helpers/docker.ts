import { exec as origExec } from 'child_process';
import { existsSync } from 'fs';
import { mkdir } from 'fs/promises';
import { join } from 'path';
import { get, set } from '../helpers/file';
import { Apps } from '../types';
import { promisify } from 'util';

const exec = promisify(origExec);

const start = async (id: string) => {
	// Ensure that app data directory exists
	const dataDir = join(process.cwd(), 'data', id);
	if (!existsSync(dataDir))
		await mkdir(dataDir, {
			recursive: true,
		});

	// Start the app
	await exec('docker compose up -d', {
		env: { APP_DATA_DIR: dataDir },
		cwd: join('apps', id),
	});

	// Register the app as installed
	const apps = await get<Apps>('apps.json', {
		installed: [],
		stopped: [],
	});
	apps.stopped = apps.stopped.filter((app) => app !== id);
	if (!apps.installed.includes(id)) apps.installed.push(id);
	await set<Apps>('apps.json', apps);
};

const stop = async (id: string) => {
	// Stop the app
	await exec('docker compose down', { cwd: join('apps', id) });

	// Register the app as stopped
	const apps = await get<Apps>('apps.json', {
		installed: [],
		stopped: [],
	});
	apps.installed = apps.installed.filter((app) => app !== id);
	if (!apps.stopped.includes(id)) apps.stopped.push(id);
	await set<Apps>('apps.json', apps);
};

export { start, stop };
