import { Apps, Settings } from '../types';
import { get, set } from './file';

const createFunctions = <Type>(
	file: string,
	defaultValue: Type
): [() => Promise<Type>, (obj: Partial<Type>) => Promise<void>] => {
	return [
		() => get<Type>(file, defaultValue),
		async (obj: Partial<Type>) =>
			set<Type>(file, {
				...(await get<Type>(file, defaultValue)),
				...obj,
			}),
	];
};

const [getSettings, setSettings] = createFunctions<Settings>('settings.json', {
	showClock: false,
	darkMode: false,
	dashStatusDots: false,
	widgets: [],
	forwarding: {},
});

const [getApps, setApps] = createFunctions<Apps>('apps.json', {
	installed: [],
	stopped: [],
});

export { getSettings, setSettings, getApps, setApps };
