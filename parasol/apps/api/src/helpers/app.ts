import { existsSync } from 'fs';
import { readFile } from 'fs/promises';
import { join } from 'path';
import { z } from 'zod';
import { App } from '../types';

const appValidator = z.string().refine((val) => existsSync(join('apps', val)));

const readConfig = async (appId: string): Promise<App> => {
	const app = JSON.parse(
		await readFile(join('apps', appId, 'app.json'), 'utf8')
	);

	// @ts-ignore
	if (app.$schema) delete app.$schema;

	return app as App;
}

export { appValidator, readConfig };
