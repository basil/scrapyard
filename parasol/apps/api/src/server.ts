import * as trpc from '@trpc/server';
import * as trpcExpress from '@trpc/server/adapters/express';
import express from 'express';
import { App, DockerQuery, Settings } from './types';
import { z } from 'zod';
import { existsSync } from 'fs';
import { readFile, readdir, rm } from 'fs/promises';
import { join } from 'path';
import { start, stop } from './helpers/docker';
import { appValidator, readConfig } from './helpers/app';
import { exec } from 'child_process';
import { getApps, getSettings, setApps, setSettings } from './helpers/config';

// TODO: Split this up into multiple files?

const app = express();
const appRouter = trpc
	.router()
	.query('getSettings', {
		resolve: async (): Promise<Settings> => {
			const settings = await getSettings();
			const apps = await readdir('apps');

			return {
				...settings,
				// Filter widgets to ensure that they're valid.
				widgets: settings.widgets.filter(
					(app) =>
						// If it's not an app, let it through. Currently, this'll never happen,
						// but it exists as a placeholder for building checks for widgets.
						!app.startsWith('app/') ||
						// If it is an app, make sure it exists.
						(app.startsWith('app/') &&
							apps.includes(app.replace('app/', '')))
				),
			};
		},
	})
	.mutation('setSettings', {
		input: z.object({
			showClock: z.boolean(),
			darkMode: z.boolean(),
			dashStatusDots: z.boolean(),
			widgets: z.array(z.string()),
			forwarding: z.record(
				z.union([
					z.object({
						type: z.literal('local'),
					}),
					z.object({
						type: z.literal('domain'),
						domain: z.string(),
					}),
				])
			),
		}),
		resolve: ({ input }) => setSettings(input as Settings),
	})
	.query('getIcon', {
		input: z
			.string()
			.refine((val) => existsSync(join('apps', val, 'icon.svg'))),
		resolve: async ({ input }) =>
			(await readFile(join('apps', input, 'icon.svg'), 'utf8')).replace(
				/black/g,
				'currentColor'
			),
	})
	.query('getAllApps', {
		resolve: async (): Promise<App[]> => {
			// It might be wise to implement some sort of caching.
			// Sorting as well, currently apps are returned in an unknown order.

			const apps = await getApps();

			const folders = (await readdir('apps')).filter(
				(app) => !app.startsWith('_')
			);
			const appList = await Promise.all<App>(
				folders.map<Promise<App>>(async (folder) => {
					const app = await readConfig(folder);

					return {
						...app,
						dependencies: app.dependencies || [],
						requirements: app.requirements || [],
						id: folder,
						installed:
							apps.installed.includes(folder) ||
							apps.stopped.includes(folder),
						running: apps.installed.includes(folder),
					};
				})
			);

			return appList;
		},
	})
	.mutation('install', {
		input: z.object({
			app: appValidator,
			dependencies: z
				.object({
					domain: z.union([
						z.object({
							type: z.literal('local'),
						}),
						z.object({
							type: z.literal('domain'),
							domain: z.string(),
						}),
					]),
				})
				// .partial(),
		}),
		resolve: async ({ input }) => {
			// TODO: Validate dependencies
			console.log(__dirname);
			const { app, dependencies } = input;
			// const config =
			// Add forwarding setting
			// if (dependencies)

			try {
				await start(app);

				// Add widget
				const settings = await getSettings();
				await setSettings({
					widgets: [...settings.widgets, `app/${app}`],
					forwarding: {
						...settings.forwarding,
						...(dependencies.domain && {
							[app]: dependencies.domain,
						}),
					},
				});
			} catch (e) {
				console.log(e);
				throw new trpc.TRPCError({
					code: 'INTERNAL_SERVER_ERROR',
					message: 'failed to install app',
					cause: e,
				});
			}
		},
	})
	.mutation('uninstall', {
		input: appValidator,
		resolve: async ({ input }) => {
			try {
				await stop(input);

				await rm(join(process.cwd(), 'data', input), {
					force: true,
					recursive: true,
				});

				// Ensure that the app isn't in apps.json
				const apps = await getApps();
				await setApps({
					stopped: apps.stopped.filter((app) => app !== input),
					// Realistically, we only need to do this for the "stopped" array, but let's be safe.
					installed: apps.installed.filter((app) => app !== input),
				});

				// Remove widget
				const settings = await getSettings();
				delete settings.forwarding[input];
				await setSettings({
					widgets: settings.widgets.filter(
						(widget) => widget !== `app/${input}`
					),
					forwarding: settings.forwarding
				});
			} catch (e) {
				throw new trpc.TRPCError({
					code: 'INTERNAL_SERVER_ERROR',
					message: 'failed to uninstall app',
					cause: e,
				});
			}
		},
	})
	.mutation('start', {
		input: appValidator,
		resolve: async ({ input }) => {
			try {
				await start(input);
			} catch (e) {
				throw new trpc.TRPCError({
					code: 'INTERNAL_SERVER_ERROR',
					message: 'failed to start app',
					cause: e,
				});
			}
		},
	})
	.mutation('stop', {
		input: appValidator,
		resolve: async ({ input }) => {
			try {
				await stop(input);
			} catch (e) {
				throw new trpc.TRPCError({
					code: 'INTERNAL_SERVER_ERROR',
					message: 'failed to stop app',
					cause: e,
				});
			}
		},
	})
	.query('getDocker', {
		resolve: async () =>
			new Promise<DockerQuery>((resolve) => {
				// Get docker version
				exec('docker -v', (err, out) => {
					if (err)
						return resolve({
							error: true,
							running: false,
							version: 'unknown',
						});

					const match = out.match(/\d+\.\d+\.\d+/);
					if (!match)
						return resolve({
							error: true,
							running: false,
							version: 'unknown',
						});

					const version = match[0];

					exec('docker info', (_, out) =>
						resolve({
							version,
							// Hacky, but I couldn't find a better/officially documented way.
							running: out.includes('Containers:'),
						})
					);
				});
			}),
	});

app.use('/api', trpcExpress.createExpressMiddleware({ router: appRouter }));

app.listen(3001, () => console.log('API Listening on port 3001'));

export type AppRouter = typeof appRouter;
