import { AppRouter } from './server';

type App = {
	name: string;
	id: string;
	version: string;
	description: string;
	port: string;
	dependencies: string[];
	requirements: Requirement[];
	installed: boolean;
	running: boolean;
};

type Requirement = 'domain';

type Apps = {
	installed: string[];
	stopped: string[];
};

type Forwarding = { type: 'local' } | { type: 'domain'; domain: string };

type Settings = {
	darkMode: boolean;
	showClock: boolean;
	dashStatusDots: boolean;
	widgets: string[];
	forwarding: Record<string, Forwarding>;
};

type DockerQuery = {
	version: string;
	running: boolean;
	error?: boolean;
};

export type { Settings, AppRouter, DockerQuery, Apps, App, Forwarding };
