
## Port Dibs

Which apps have registered what ports.

| App         | Port |
|-------------|------|
| Uptime Kuma | 8000 |
| Pi-hole     | 8001 |
| Gitea       | 8002 |

## Settings

When adding a setting, make sure you:
- Update `src/types.ts` (`Settings` type).
- Update `src/server.ts` (`setSettings` mutation and `getSettings` query).
- Update or delete `config/settings.json`, at least in development.
