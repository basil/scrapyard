import { defineConfig } from 'vite';
import solidPlugin from 'vite-plugin-solid';
import { join } from 'path';

export default defineConfig({
	plugins: [solidPlugin()],
	build: {
		target: 'esnext',
		polyfillDynamicImport: false,
	},
	optimizeDeps: {
		exclude: ['@trpc/client'],
	},
	resolve: {
		alias: {
			'~': join(__dirname, './src'),
		},
	},
	server: {
		proxy: {
			'/api': 'http://localhost:3001',
		},
	},
});
