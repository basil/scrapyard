import { Component } from 'solid-js';
import { Routes, Route } from 'solid-app-router';
import Inspect from 'solid-inspx';
import { StoreProvider } from '~/helpers/store/store';
import useTheme from '~/helpers/useTheme';

import Home from '~/pages';
import Settings from '~/pages/settings';
import Apps from '~/pages/apps';
import Alert from './components/Alert';

const App: Component = () => {
	return (
		<Inspect>
			<StoreProvider>
				{() => {
					// It might be better to extract this into something that can run without flashing.
					// This has to be inside of the global context to run.
					useTheme();
					return null;
				}}
				<Alert />
				<Routes>
					<Route path='/' element={<Home />} />
					<Route path='/settings' element={<Settings />} />
					<Route path='/apps' element={<Apps />} />
				</Routes>
			</StoreProvider>
		</Inspect>
	);
};

export default App;
