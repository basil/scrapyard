import { Component, createSignal } from 'solid-js';
import { For, Show } from 'solid-js';
import Page from '~/components/Page';
import Icon from '~/components/Icon';
import { useGlobalContext } from '~/helpers/store/store';
import Status from '~/components/Status';

type DashboardApp = {
	type: 'app';
	name: string;
	icon: string;
	online: boolean;
	url: string;
};

const Home: Component = () => {
	const [items, setItems] = createSignal<DashboardApp[]>([]);
	const {
		settingsService: { settings },
		appsService: { apps },
	} = useGlobalContext();

	setItems(
		settings.widgets.map<DashboardApp>((item) => {
			const id = item.replace('app/', '');
			const app = apps.find((app) => app.id === id)!;

			return {
				type: 'app',
				name: app.name,
				icon: item.replace('app/', ''),
				online: app.running,
				url: `http://127.0.0.1:${app.port}`,
			};
		})
	);

	return (
		<Page class='flex flex-wrap gap-3'>
			<Show
				when={items().length}
				fallback={
					<>
						<p class='m-auto text-darkGray'>
							no widgets, install some apps or configure widgets
							in the settings.
						</p>
					</>
				}
			>
				<For each={items()}>
					{(item) => (
						<>
							<Show when={item.type === 'app'}>
								{item.type === 'app' && (
									<a
										class='flex h-2c w-2c flex-col justify-between border-1 border-black bg-white p-2'
										href={item.url}
									>
										<div class='flex items-center gap-1'>
											<p>{item.name}</p>
											<Show
												when={settings.dashStatusDots}
											>
												<Status
													status={
														item.online
															? 'online'
															: 'offline'
													}
												/>
											</Show>
										</div>

										<Icon
											class='mx-auto h-13 w-13'
											id={item.icon}
										/>
									</a>
								)}
							</Show>
						</>
					)}
				</For>
			</Show>
		</Page>
	);
};

export default Home;
