import { Component } from 'solid-js';
import Page from '~/components/Page';
import Sidebar from '~/components/Sidebar';
import {
	Appearance,
	Widgets,
	System,
	Forwarding,
} from '~/components/SettingsPages';

const Settings: Component = () => (
	<Page>
		<Sidebar
			default={0}
			entries={[
				['appearance', <Appearance />],
				['widgets', <Widgets />],
				['forwarding', <Forwarding />],
				['system', <System />],
			]}
		/>
	</Page>
);

export default Settings;
