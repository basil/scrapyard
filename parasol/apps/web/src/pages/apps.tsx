import { ChevronLeft } from 'lucide-solid';
import { Component, createSignal, For, Show } from 'solid-js';
import { createStore, produce } from 'solid-js/store';
import { alert, dismiss } from '~/components/Alert';
import Button from '~/components/Button';
import Icon from '~/components/Icon';
import Logo from '~/components/Icon';
import Page from '~/components/Page';
import { useGlobalContext } from '~/helpers/store/store';
import client from '~/helpers/trpc';
// Is monorepo traversal bad practice? It's a monorepo, so it'll get comitted together.
import { App as AppType, Forwarding } from 'api/src/types';
import TextInput from '~/components/TextInput';

// App actions (install, uninstall) are blocked when an action for that app is running
interface App extends AppType {
	disabled: boolean;
}

// I apologize in advance for the spaghetti code in this file

const Apps: Component = () => {
	const [entries, setEntries] = createStore<App[]>([]);
	// No routing here, it's not worth the added complexity.
	const [activeApp, setActiveApp] = createSignal<App>();
	const {
		appsService: { apps, updateApps },
	} = useGlobalContext();

	setEntries(
		(apps as any).map((app: App) => ({
			...app,
			disabled: false,
		}))
	);

	return (
		<Page>
			<Show
				when={activeApp()}
				fallback={
					// When no app is displayed, show all of the apps
					<>
						<p class='mb-1 text-darkGray'>app library</p>
						<Show
							when={entries.length}
							fallback={
								<p class='text-darkGray'>loading apps...</p>
							}
						>
							<div class='flex flex-wrap gap-3'>
								<For each={entries}>
									{(app) => (
										<button
											class='flex h-10 w-3c gap-2 border-1 border-black p-2'
											onClick={() => {
												setActiveApp(app);
											}}
										>
											<Logo id={app.id} class='h-6 w-6' />
											<div class='text-left'>
												<p>{app.name}</p>
												<p class='text-darkGray'>
													{app.version}
												</p>
											</div>
										</button>
									)}
								</For>
							</div>
						</Show>
					</>
				}
			>
				{() => {
					const app = activeApp();
					if (!app) return null;

					const runningCallback = () => {
						setEntries(
							(entry) => entry.id === app.id,
							'disabled',
							true
						);
						client
							.mutation(app.running ? 'stop' : 'start', app.id)
							.then(() => {
								setEntries(
									(entry) => entry.id === app.id,
									produce(
										(entry) => (
											(entry.running = !entry.running),
											(entry.disabled = false)
										)
									)
								);
								updateApps(
									(entry) => entry.id === app.id,
									'running',
									app.running
								);
							})
							.catch((err) => {
								alert(err.message);
								setEntries(
									(entry) => entry.id === app.id,
									'disabled',
									false
								);
							});
					};

					const installCallback = () => {
						// Executes the installation/uninstallation
						const run = async () => {
							setEntries(
								(entry) => entry.id === app.id,
								'disabled',
								true
							);

							if (app.installed)
								// Uninstall the app
								client
									.mutation('uninstall', app.id)
									.then(() =>
										setEntries(
											(entry) => entry.id === app.id,
											produce(
												(entry) => (
													(entry.installed =
														!app.installed),
													(entry.running = true),
													(entry.disabled = false)
												)
											)
										)
									)
									.catch((err) => {
										alert(err.message);
										setEntries(
											(entry) => entry.id === app.id,
											'disabled',
											false
										);
									});
							else {
								// Install the app
								const domain = await new Promise<Forwarding>(
									(resolve) => {
										let domain = '';
										alert(
											<>
												<p>
													{app.requirements.includes(
														'domain'
													)
														? `${app.name} requires a domain. you cannot change this later.`
														: 'configure a domain? you cannot change this later.'}
												</p>

												<TextInput
													class='mt-1'
													placeholder={`${app.id}.example.com`}
													onChange={(e) =>
														(domain = (
															e.target as HTMLInputElement
														).value)
													}
												/>
											</>,
											[
												{
													text: 'confirm',
													callback: () =>
														resolve({
															type: 'domain',
															domain,
														}),
												},
												...(!app.requirements.includes(
													'domain'
												)
													? [
															{
																text: 'skip',
																callback: () =>
																	resolve({
																		type: 'local',
																	}),
															},
													  ]
													: []),
											]
										);
									}
								);

								client
									.mutation('install', {
										app: app.id,
										dependencies: { domain },
									})
									.then(() =>
										setEntries(
											(entry) => entry.id === app.id,
											produce(
												(entry) => (
													(entry.installed =
														!app.installed),
													(entry.running = true),
													(entry.disabled = false)
												)
											)
										)
									)
									.catch((err) => {
										alert(err.message);
										setEntries(
											(entry) => entry.id === app.id,
											'disabled',
											false
										);
									});

								// client
								// 	.mutation(
								// 		app.installed ? 'uninstall' : 'install',
								// 		app.installed
								// 			? app.id
								// 			: {
								// 					app: app.id,
								// 					dependencies: {},
								// 			  }
								// 	)
								// 	.then(() => {
								// 		setEntries(
								// 			(entry) => entry.id === app.id,
								// 			produce(
								// 				(entry) => (
								// 					(entry.installed =
								// 						!app.installed),
								// 					(entry.running = true),
								// 					(entry.disabled = false)
								// 				)
								// 			)
								// 			// !app.installed
								// 		);
								// 		// setEntries(
								// 		// 	(entry) => entry.id === app.id,
								// 		// 	'installed',
								// 		// 	!app.installed
								// 		// );
								// 		// setEntries(
								// 		// 	(entry) => entry.id === app.id,
								// 		// 	'running',
								// 		// 	true
								// 		// );
								// 		// setEntries(
								// 		// 	(entry) => entry.id === app.id,
								// 		// 	'disabled',
								// 		// 	false
								// 		// );
								// 	})
								// 	.catch((err) => {
								// 		alert(err.message);
								// 		setEntries(
								// 			(entry) => entry.id === app.id,
								// 			'disabled',
								// 			false
								// 		);
								// 	});
							}
						};

						// The actual logic of deciding whether or not to install or uninstall an app.
						if (app.installed)
							alert(
								<p>
									uninstalling will delete all data associated
									with <b>{app.name}</b>. are you sure you
									want to proceed?
								</p>,
								[
									{
										text: 'yep, uninstall',
										callback: run,
									},
									{
										text: 'no thanks',
									},
								]
							);
						else {
							if (
								!app.dependencies.every((dep) =>
									entries.some(
										(app) => app.id === dep && app.installed
									)
								)
							) {
								alert(
									<>
										<p>
											The following apps are needed, but
											are not installed:
										</p>
										<For each={app.dependencies}>
											{(dep) => (
												<a
													class='mt-1 flex cursor-pointer gap-1'
													onClick={() => {
														setActiveApp(
															entries.find(
																(app) =>
																	app.id ===
																	dep
															)
														);
														dismiss();
													}}
												>
													<Icon
														id={dep}
														class='h-3 w-3'
													/>
													<p>
														{
															entries.find(
																(app) =>
																	app.id ===
																	dep
															)!.name
														}
													</p>
												</a>
											)}
										</For>
									</>
								);
							} else run();
						}
					};

					return (
						<>
							<button
								class='mb-3 flex items-center gap-1 stroke-darkGray text-darkGray'
								onClick={() => setActiveApp()}
							>
								<ChevronLeft />
								back
							</button>
							<div class='mb-3 flex gap-2'>
								<Logo id={activeApp()!.id} class='h-8 w-8' />
								<div>
									<div class='mb-1 flex gap-1'>
										<p>{activeApp()!.name}</p>
										<p class='text-darkGray'>
											{activeApp()!.version}
										</p>
									</div>

									<Button
										class='mr-1'
										disabled={app.disabled}
										onClick={installCallback}
									>
										{app.installed
											? 'uninstall'
											: 'install'}
									</Button>

									<Show when={app.installed}>
										<Button
											class='mr-1'
											disabled={app.disabled}
											onClick={runningCallback}
										>
											{app.running ? 'stop' : 'start'}
										</Button>
										{/* TODO: Add support for alternate root domain */}
										<a
											href={`${document.location.protocol}//${document.location.hostname}:${app.port}`}
											target='_blank'
										>
											<Button disabled={app.disabled}>
												open
											</Button>
										</a>
									</Show>
								</div>
							</div>
							<p class='text-darkGray'>description</p>
							<p>{app.description}</p>
						</>
					);
				}}
			</Show>
		</Page>
	);
};

export default Apps;
