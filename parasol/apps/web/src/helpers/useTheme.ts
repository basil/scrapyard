import { createEffect } from 'solid-js';
import { useGlobalContext } from '~/helpers/store/store';

// Adapted from https://stackoverflow.com/a/39077686
const formatHex = (hex: string) =>
	hex
		.replace(
			/^#?([a-f\d])([a-f\d])([a-f\d])$/i,
			(m, r, g, b) => '#' + r + r + g + g + b + b
		)
		.substring(1)
		.match(/.{2}/g)!
		.map((x) => parseInt(x, 16)).join(' ');

const useTheme = () => {
	const {
		settingsService: { settings },
	} = useGlobalContext();

	createEffect(() => {
		const variables = {
			black: formatHex(settings.darkMode ? '#FFFFFF' : '#000000'),
			darkGray: formatHex(settings.darkMode ? '#CDCDCD': '#656565'),
			lightGray: formatHex(settings.darkMode ? '#656565': '#CDCDCD'),
			white: formatHex(settings.darkMode ? '#000000' : '#FFFFFF'),
			green: formatHex('#12BE38'),
			red: formatHex('#D81212'),
		};

		for (const variable in variables) {
			document.documentElement.style.setProperty(
				`--${variable}`,
				(variables as any)[variable]
			);
		}
	});
};

export default useTheme;
