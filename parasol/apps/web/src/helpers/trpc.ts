import { AppRouter } from 'api';
import { createTRPCClient } from '@trpc/client';

const client = createTRPCClient<AppRouter>({
	url: '/api',
});

export default client;
