import { Settings } from 'api';
import { onMount } from 'solid-js';
import { createStore } from 'solid-js/store';
import client from '~/helpers/trpc';

// @ts-ignore because we ensure that it isn't null before rendering any components
const settingsStore = createStore<Settings>();

const SettingsService = () => {
	const [settings, setSettings] = settingsStore;

	onMount(() => {
		// Asynchronously fetch settings
		client.query('getSettings').then((settings) => {
			setSettings(settings);
			localStorage.setItem('settings', JSON.stringify(settings));
		});

		// Get "cached" settings if available
		const settingsString = localStorage.getItem('settings');
		if (!settingsString) return;
		setSettings(() => JSON.parse(settingsString) as unknown as Settings);
	});

	const updateSettings = async (apps: Settings) => {
		setSettings(apps);
		localStorage.setItem('settings', JSON.stringify(apps));
		// "cache" settings
		await client.mutation('setSettings', apps);
	};

	return { settings, updateSettings };
};

export { SettingsService };
