import { App } from 'api';
import { onMount } from 'solid-js';
import { createStore } from 'solid-js/store';
import client from '~/helpers/trpc';

const appsStore = createStore<App[]>([]);

const AppsService = () => {
	const [apps, setApps] = appsStore;

	onMount(() => {
		// Asynchronously fetch settings
		client.query('getAllApps').then(setApps);
	});
	
	return { apps, updateApps: setApps };
};

export { AppsService };
