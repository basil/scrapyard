import { createStore } from 'solid-js/store';
import client from '~/helpers/trpc';

const iconsStore = createStore<Record<string, string>>();

// Potentially could be integrated with IndexedDB and check for updates via hashes, but that seems like overkill
const IconsService = () => {
	const [icons, setSettings] = iconsStore;

	const getIcon = async (id: string) => {
		if (icons[id]) return icons[id];

		// Note that if the client fails to fetch an icon for some reason, it will try again.
		try {
			const icon = await client.query('getIcon', id);
			setSettings(id, icon);
			return icon;
		} catch {
			return '';
		}
	};

	return { getIcon };
};

export { IconsService };
