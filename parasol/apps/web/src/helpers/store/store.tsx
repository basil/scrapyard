import { ParentComponent, Show } from 'solid-js';
import { createContext, useContext } from 'solid-js';
import { Umbrella } from 'lucide-solid';
import { SettingsService } from './services/settingsService';
import { IconsService } from './services/iconsService';
import { AppsService } from './services/appsService';

type RootState = {
	settingsService: ReturnType<typeof SettingsService>;
	iconsService: ReturnType<typeof IconsService>;
	appsService: ReturnType<typeof AppsService>;
};

const rootState: RootState = {
	// Settings, "cached" locally for speed
	settingsService: SettingsService(),
	// Icons for apps, stored as SVG strings
	iconsService: IconsService(),
	// App data
	appsService: AppsService(),
};

const StoreContext = createContext<RootState>();

const useGlobalContext = () => useContext(StoreContext)!;

const StoreProvider: ParentComponent = (props) => (
	<Show
		// Is there a better way to do this?
		when={
			Object.keys(rootState.settingsService.settings).length &&
			rootState.appsService.apps.length
		}
		fallback={
			<div class='absolute inset-0 z-50 grid place-items-center bg-white'>
				<Umbrella class='h-10 w-10 stroke-lightGray' />
			</div>
		}
	>
		<StoreContext.Provider value={rootState}>
			{props.children}
		</StoreContext.Provider>
	</Show>
);

export type { RootState };
export { useGlobalContext, StoreProvider };
