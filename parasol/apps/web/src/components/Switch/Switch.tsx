import { ParentComponent } from 'solid-js';
import { Checkbox, CheckboxIndicator, CheckboxLabel } from 'solid-headless';
import { Show } from 'solid-js';

type SwitchProps = {
	checked: boolean;
	setChecked: (val: boolean) => void;
};

const Switch: ParentComponent<SwitchProps> = (props) => (
	<div class='flex items-center gap-1'>
		<Checkbox
			checked={props.checked}
			onChange={(val) => {
				if (val !== undefined) props.setChecked(val);
			}}
		>
			<CheckboxIndicator
				class={`relative h-2 w-4 rounded-1 ${
					props.checked ? 'bg-black' : 'bg-white'
				}`}
			>
				<div class='absolute inset-0 rounded-1 border-1 border-black' />
				<div
					class={`absolute top-0 box-border h-2 w-2 rounded-1 border-1 border-black bg-white ${
						props.checked ? 'right-0' : 'left-0'
					}`}
				/>
			</CheckboxIndicator>
			<Show when={props.children}>
				<CheckboxLabel>{props.children}</CheckboxLabel>
			</Show>
		</Checkbox>
	</div>
);

export default Switch;
