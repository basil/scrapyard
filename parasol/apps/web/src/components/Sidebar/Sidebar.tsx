import { Component, JSX, Show } from 'solid-js';
import { For, createSignal } from 'solid-js';

type SidebarProps = {
	entries: [string, JSX.Element][];
	default: number;
};

const Sidebar: Component<SidebarProps> = (props) => {
	const [selected, setSelected] = createSignal(props.default);

	return (
		<div class='flex gap-3'>
			<nav class='flex w-2c flex-col gap-1 min-w-max'>
				<For each={props.entries}>
					{(entry, i) => (
						<button
							class={`text-left ${
								selected() === i()
									? 'text-black'
									: 'text-darkGray'
							}`}
							onClick={() => setSelected(i())}
						>
							{entry[0]}
						</button>
					)}
				</For>
			</nav>

			<For each={props.entries}>
				{(_, i) => (
					<Show when={selected() === i()}>
						<div class='max-w-10c'>{props.entries[selected()][1]}</div>
					</Show>
				)}
			</For>
		</div>
	);
};

export default Sidebar;
