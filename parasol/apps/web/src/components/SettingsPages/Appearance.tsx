import { Component } from 'solid-js';
import Switch from '~/components/Switch';
import { useGlobalContext } from '~/helpers/store/store';

const Appearance: Component = () => {
	const {
		settingsService: { settings, updateSettings },
	} = useGlobalContext();

	return (
		<>
			<Switch
				checked={settings.darkMode}
				setChecked={(val) =>
					updateSettings({
						...settings,
						darkMode: val,
					})
				}
			>
				dark mode
			</Switch>
			<Switch
				checked={settings.showClock}
				setChecked={(val) =>
					updateSettings({
						...settings,
						showClock: val,
					})
				}
			>
				show clock
			</Switch>
			<Switch
				checked={settings.dashStatusDots}
				setChecked={(val) =>
					updateSettings({
						...settings,
						dashStatusDots: val,
					})
				}
			>
				show app status indicators on dashboard
			</Switch>
		</>
	);
};

export default Appearance;
