import Switch from '~/components/Switch';
import { Component, For } from 'solid-js';
import { createStore } from 'solid-js/store';
import { GripVertical } from 'lucide-solid';
import {
	closestCenter,
	createSortable,
	DragDropProvider,
	DragDropSensors,
	DragEventHandler,
	SortableProvider,
	transformStyle,
} from '@thisbeyond/solid-dnd';
import { useGlobalContext } from '~/helpers/store/store';

type Item = {
	type: 'app';
	text: string;
	id: string;
	checked: boolean;
};

const Widget: Component<{
	item: Item;
	setChecked: (bool: boolean) => void;
}> = (props) => {
	const sortable = createSortable(props.item.text);

	const updateChecked = (val: boolean) => props.setChecked(val);

	return (
		<div
			class={`sortable flex items-center gap-1 ${
				sortable.isActiveDraggable ? 'opacity-25' : ''
			}`}
			ref={sortable.ref}
			style={transformStyle(sortable.transform)}
		>
			<GripVertical
				class='cursor-grab stroke-1 active:cursor-grabbing [&>*]:fill-black'
				{...sortable.dragActivators}
			/>
			<Switch checked={props.item.checked} setChecked={updateChecked}>
				<p class='text-black'>{props.item.text}</p>
			</Switch>
		</div>
	);
};

// TODO: Unify up type definition to "Widget" instead of "App"
const Apparance: Component = () => {
	const [items, setItems] = createStore<Item[]>([]);
	const {
		settingsService: { settings, updateSettings },
		appsService: { apps },
	} = useGlobalContext();

	// Update the widget settings
	// Has optional value for when called during drag-and-drop as to not desync
	const update = (newItems?: Item[]) =>
		updateSettings({
			...settings,
			...{
				widgets: (newItems || items)
					.filter((item) => item.checked)
					.map((item) => `${item.type}/${item.id}`),
			},
		});

	const enabledWidgets = settings.widgets
		.filter((widget) => widget.startsWith('app/'))
		.map((widget) => widget.replace('app/', ''));

	const widgets: Item[] = apps
		.filter((app) => app.installed)
		.sort(
			(a, b) =>
				enabledWidgets.indexOf(a.id) - enabledWidgets.indexOf(b.id)
		)
		.map((app) => ({
			type: 'app',
			text: app.name,
			id: app.id,
			checked: enabledWidgets.includes(app.id),
		}));

	setItems([
		...widgets.filter((app) => app.checked),
		...widgets.filter((app) => !app.checked),
	]);

	const onDragEnd: DragEventHandler = ({ draggable, droppable }) => {
		if (draggable && droppable) {
			const currentItems = items;
			const fromIndex = currentItems.findIndex(
				(item) => item.text === draggable.id
			);
			const toIndex = currentItems.findIndex(
				(item) => item.text === droppable.id
			);

			if (fromIndex !== toIndex) {
				const updatedItems = currentItems.slice();
				updatedItems.splice(
					toIndex,
					0,
					...updatedItems.splice(fromIndex, 1)
				);

				setItems(updatedItems);
				update(updatedItems);
			}
		}
	};

	return (
		// @ts-ignore solid-dnd on NPM hasn't been updated with the new Solid ParentComponent type
		<DragDropProvider
			onDragEnd={onDragEnd}
			collisionDetector={closestCenter}
		>
			<DragDropSensors />
			<div class='flex flex-col gap-1'>
				{/* @ts-ignore ditto */}
				<SortableProvider ids={items.map((item) => item.text)}>
					<For
						each={items}
						fallback={
							<p class='text-darkGray'>
								install apps to configure widgets.
							</p>
						}
					>
						{(item) => (
							<Widget
								item={item}
								setChecked={(bool) => {
									setItems(
										(entry) => entry.text === item.text,
										'checked',
										bool
									);
									update();
								}}
							/>
						)}
					</For>
				</SortableProvider>
			</div>
		</DragDropProvider>
	);
};

export default Apparance;
