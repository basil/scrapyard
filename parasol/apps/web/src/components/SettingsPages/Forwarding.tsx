import { AlertTriangle } from 'lucide-solid';
import { Component, createSignal, For, Show } from 'solid-js';
import { useGlobalContext } from '~/helpers/store/store';
import Button from '~/components/Button';
import RadioButton from '~/components/RadioButton';
import TextInput from '../TextInput/TextInput';

const Forwarding: Component = () => {
	const {
		settingsService: { settings, updateSettings },
		appsService: { apps },
	} = useGlobalContext();

	return (
		<>
			<p class='mb-1 text-darkGray'>forwarding</p>

			<div class='mb-1 border-1 border-black p-2'>
				<div class='mb-1 flex items-center gap-1'>
					<AlertTriangle />
					<p>important!</p>
				</div>
				<p>
					apps with a <AlertTriangle class='inline' /> symbol have
					specified that they need a specific hostname, and changing
					it here without tweaking the app’s configuration files may
					break the app. alternatively, you can uninstall (which will
					wipe the app’s data) and reinstall the app, which will
					prompt you for a new host with no risk of breaking the app.
				</p>
			</div>

			<p class='mb-1'>
				local - no forwarding, accessible with the ip and port.
			</p>
			<p class='mb-1'>
				domain - the app will expect to be accessible at that domain,
				but configuring DNS is up to you.
			</p>

			<For each={Object.keys(settings.forwarding)}>
				{(key) => {
					const app = apps.find((app) => app.id === key)!;
					const [updating, setUpdating] = createSignal(false);
					const [domain, setDomain] = createSignal(
						settings.forwarding[app.id].type === 'domain'
							? // @ts-ignore We ensure that the domain is used if it exists
							  settings.forwarding[app.id].domain
							: ''
					);
					const [domainSelected, setDomainSelected] = createSignal(
						settings.forwarding[app.id].type === 'domain'
					);

					return (
						<div class='mt-3 flex w-max flex-col gap-1'>
							<div class='flex items-center gap-1'>
								<p>{app.name}</p>
								{app.requirements.includes('domain') && (
									<AlertTriangle />
								)}
							</div>

							<div class='flex items-center gap-1'>
								<RadioButton
									name={`${app.name}-forwarding`}
									id={`${app.name}-forwarding-local`}
									value='local'
									checked={
										settings.forwarding[app.id].type ===
										'local'
									}
									disabled={updating()}
									onClick={async () => {
										setUpdating(true);

										setDomainSelected(false);
										await updateSettings({
											...settings,
											forwarding: {
												[app.id]: {
													type: 'local',
												},
											},
										});

										setUpdating(false);
									}}
								/>
								<label for={`${app.name}-forwarding-local`}>
									local
								</label>
							</div>
							<div class='flex items-center gap-1'>
								<RadioButton
									name={`${app.name}-forwarding`}
									id={`${app.name}-forwarding-domain`}
									value='domain'
									checked={
										settings.forwarding[app.id].type ===
										'domain'
									}
									disabled={updating()}
									onClick={() => setDomainSelected(true)}
								/>
								<label for={`${app.name}-forwarding-domain`}>
									domain
								</label>
							</div>
							<Show when={domainSelected()}>
								<div class='flex flex-wrap gap-1'>
									<TextInput
										placeholder={`${app.id}.example.com`}
										onChange={(e) =>
											setDomain(
												(e.target as HTMLInputElement)
													.value
											)
										}
										value={domain()}
									/>
									<Button
										class='h-5'
										disabled={updating()}
										onClick={async () => {
											if (!domain()) return;
											setUpdating(true);

											await updateSettings({
												...settings,
												forwarding: {
													[app.id]: {
														type: 'domain',
														domain: domain(),
													},
												},
											});

											setUpdating(false);
										}}
									>
										update
									</Button>
								</div>
							</Show>
						</div>
					);
				}}
			</For>
		</>
	);
};

export default Forwarding;
