import { DockerQuery } from 'api';
import { Component, createSignal, Show } from 'solid-js';
import client from '~/helpers/trpc';
import pkg from '../../../package.json';
import Status from '~/components/Status';

type SystemProps = {};

const System: Component<SystemProps> = (props) => {
	const [docker, setDocker] = createSignal<DockerQuery>();

	client.query('getDocker').then(setDocker);

	return (
		<>
			<p>parasol v{pkg.version}</p>
			<Show
				when={docker() !== undefined}
				fallback={<p class='text-darkGray'>loading Docker status...</p>}
			>
				{() => {
					// Is this properly reactive?
					// A bit of a hack, but I'm on a plane and can't look stuff up
					const dock = docker();
					if (dock === undefined) return <></>;

					// If an error ocurred, meaning we have null, we probably couldn't find Docker
					if (dock.error)
						return (
							<div class='flex items-center gap-1'>
								<Status status='error' />
								<p>Docker not found</p>
							</div>
						);

					return (
						<div class='flex items-center gap-1'>
							<Status
								status={dock.running ? 'online' : 'offline'}
							/>
							<p>
								Docker v{dock.version}
								{!dock.running && ' - engine stopped'}
							</p>
						</div>
					);
				}}
			</Show>
		</>
	);
};

export default System;
