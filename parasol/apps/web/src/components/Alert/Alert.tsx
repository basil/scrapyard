import { Component, JSX } from 'solid-js';
import { Dialog, DialogOverlay, DialogPanel } from 'solid-headless';
import { createSignal, For, Show } from 'solid-js';
import Button from '../Button';

type AlertProps = {};

type Alert = {
	content: string | JSX.Element;
	actions: {
		text: string;
		callback?: () => void;
	}[];
};

const [alerts, setAlerts] = createSignal<Alert[]>([]);
const alert = (
	content: string | JSX.Element,
	actions?: {
		text: string;
		callback?: () => void;
	}[]
) =>
	setAlerts([
		...alerts(),
		{
			content,
			actions: actions || [
				{
					text: 'ok',
				},
			],
		},
	]);
const dismiss = () => setAlerts(alerts().slice(1));

const Alert: Component<AlertProps> = () => {
	const alert = () => alerts()[0];

	return (
		<Show when={alerts().length}>
			<Dialog isOpen>
				<DialogOverlay class='absolute inset-0 bg-black bg-opacity-20' />
				<DialogPanel class='absolute inset-0 m-auto h-fit w-4c border-1 border-black bg-white p-2'>
					<div class='mb-4'>
						{typeof alert().content === 'string' ? (
							<p>{alert().content}</p>
						) : (
							<p>{alert().content}</p>
						)}
					</div>

					<For each={alert().actions}>
						{(action) => (
							<Button
								class='mr-1'
								onClick={() => {
									if (action.callback) action.callback();
									setAlerts(alerts().slice(1));
								}}
							>
								{action.text}
							</Button>
						)}
					</For>
				</DialogPanel>
			</Dialog>
		</Show>
	);
};

export default Alert;
export { alert, dismiss };
