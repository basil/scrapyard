import { ParentComponent } from 'solid-js';
import { LayoutGrid, Settings, ShoppingBag } from 'lucide-solid';
import { Link } from 'solid-app-router';
import Clock from '~/components/Clock';

type PageProps = {
	class?: string;
};

const Page: ParentComponent<PageProps> = (props) => (
	<>
		<div class='mb-3 flex h-3 items-center gap-1 stroke-darkGray text-darkGray'>
			<Clock />
			<Link href='/'>
				<LayoutGrid />
			</Link>
			<Link href='/apps'>
				<ShoppingBag />
			</Link>
			<Link href='/settings'>
				<Settings />
			</Link>
		</div>

		<main class={props.class}>{props.children}</main>
	</>
);

export default Page;
