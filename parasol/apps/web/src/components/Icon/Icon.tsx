import { Component, createEffect, createSignal } from 'solid-js';
import { IconsService } from '~/helpers/store/services/iconsService';

type LogoProps = {
	id: string;
	class?: string;
};

const Icon: Component<LogoProps> = (props) => {
	const [svg, setSvg] = createSignal('');
	let ref: HTMLDivElement | undefined;

	// We can't use context in alerts.
	IconsService().getIcon(props.id).then((val) => setSvg(val));

	// A bit of a hack, but I don't know of any other smart way to do dynamic loading.
	createEffect(() => {
		if (!ref || !svg()) return;

		// Make sure the SVG doesn't change stroke width when scaled
		ref.querySelectorAll('path').forEach((el) =>
			el.setAttribute('vector-effect', 'non-scaling-stroke')
		);

		// Set the class on the SVG child
		const el = ref.querySelector('svg');
		if (el && props.class) el.setAttribute('class', props.class);
	});

	// Note that both the div and the SVG get the same class
	return <div ref={ref} class={props.class} innerHTML={svg()} />;
};

export default Icon;
