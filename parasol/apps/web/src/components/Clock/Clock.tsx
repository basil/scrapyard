import { Component, createSignal, Show } from 'solid-js';
import { useGlobalContext } from '~/helpers/store/store';

// TODO: Use server time, not local time.
// TODO: Don't run logic when not active.
const Clock: Component = () => {
	const [date, setDate] = createSignal(new Date());
	const {
		settingsService: { settings },
	} = useGlobalContext();

	setTimeout(() => {
		setDate(new Date());
		setInterval(() => {
			setDate(new Date());
		}, 60 * 1000);
	}, (60 - new Date().getSeconds()) * 1000);

	return (
		<Show when={settings.showClock}>
			<p>
				{date()
					.toLocaleTimeString([], {
						hour: 'numeric',
						minute: '2-digit',
					})
					.toLowerCase()}
			</p>
		</Show>
	);
};

export default Clock;
