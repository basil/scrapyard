import { ParentComponent, JSX } from 'solid-js';

type ButtonProps = {
	disabled?: boolean;
	inverted?: boolean;
	extraPadding?: boolean;
} & JSX.ButtonHTMLAttributes<HTMLButtonElement>;

const Button: ParentComponent<ButtonProps> = (props) => (
	<button
		{...props}
		class={`h-4 rounded-0.5 border-1 border-black px-1.5 ${
			props.inverted ? 'bg-black text-white' : ''
		} ${props.disabled ? 'opacity-50' : ''} ${props.class}`}
	>
		{props.children}
	</button>
);

export default Button;
