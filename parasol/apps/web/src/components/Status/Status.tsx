import { Component } from 'solid-js';

type StatusProps = {
	status: 'online' | 'offline' | 'error';
};

const Status: Component<StatusProps> = (props) => (
	<div
		class={`h-[6px] w-[6px] rounded-0.5 ${
			{
				online: 'bg-green',
				offline: 'bg-lightGray',
				error: 'bg-red',
			}[props.status]
		}`}
	/>
);

export default Status;
