import type { Component, JSX } from 'solid-js';

const RadioButton: Component<JSX.InputHTMLAttributes<HTMLInputElement>> = (
	props
) => (
	<input
		type='radio'
		class={`form-radio h-2 w-2 border-1 !border-black bg-white !bg-none text-white checked:border-[5px] ${
			props.disabled ? 'opacity-50' : ''
		}`}
		{...props}
	/>
);

export default RadioButton;
