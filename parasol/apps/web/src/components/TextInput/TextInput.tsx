import type { Component, JSX } from 'solid-js';

const TextInput: Component<JSX.InputHTMLAttributes<HTMLInputElement>> = (
	props
) => {
	return (
		<input
			type='text'
			class={`h-5 w-3c rounded-0.5 border-1 border-black bg-white px-1.5 py-1 placeholder:text-lightGray ${
				props.disabled ? 'opacity-50' : ''
			} ${props.class || ''}`}
			{...props}
		/>
	);
};

export default TextInput;
