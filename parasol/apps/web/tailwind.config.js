/** @type {import('tailwindcss').Config} */
module.exports = {
	content: ['./index.html', './src/**/*.tsx'],
	theme: {
		extend: {},
		colors: {
			black: 'rgb(var(--black) / <alpha-value>)',
			darkGray: 'rgb(var(--darkGray) / <alpha-value>)',
			lightGray: 'rgb(var(--lightGray) / <alpha-value>)',
			white: 'rgb(var(--white) / <alpha-value>)',
			green: 'rgb(var(--green) / <alpha-value>)',
			red: 'rgb(var(--red) / <alpha-value>)',
			transparent: 'transparent',
		},
		spacing: {
			0: '0px',
			0.5: '4px',
			1: '8px',
			1.5: '12px',
			2: '16px',
			3: '24px',
			4: '32px',
			5: '40px',
			6: '48px',
			8: '64px',
			10: '80px',
			13: '104px',
			// Column widths
			'2c': '168px',
			'3c': '264px',
			'4c': '360px',
		},
		borderWidth: {
			1: '1px',
		},
		borderRadius: {
			0.5: '4px',
			1: '8px',
		},
		maxWidth: {
			'10c': '936px',
			// Page width
			141: '1128px',
		},
		fontFamily: {
			sans: ['"Inter"', 'sans-serif'],
		},
	},
	plugins: [
		require('@tailwindcss/forms'),
	],
};
